---
og:title: Software instalado
---

# 🧰 Software instalado

## LMOD
El software instalado en los clústers de cómputo depende del uso de variables de entorno para funcionar correctamente. 

Utilizamos LMod para manipular estas variables.

### Uso básico

- `module avail`: Lista los módulos disponibles.
- `module list`: Lista los módulos cargados.
- `module purge`: Limpia los módulos.
- `module unload <module name>/<version>`: Carga un módulo.

### Compiladores

Sólo podemos tener un módulo de compiladores cargado a la vez. Por ejemplo, en caso de tener cargado el módulo gnu12 que contiene los compiladores de GNU, y quisiéramos cargar los compiladores de intel mediante el comando `module load intel`, nos encontraríamos con un error.

Podemos usar el comando `swap` para intercambiar entre ellos. `module swap gnu12 intel/2023.1.0`.

### Dependencias

Algunos módulos dependen de que un módulo padre haya sido cargado previamente. Si ejecutamos el comando `module avail` solo vemos los módulos que tenemos disponibles a partir de los que ya tenemos cargados.

Por ejemplo, luego de ejecutar `module purge` nos encontramos con los módulos que no tienen ninguna dependencia.

```
[user@cluster ~]$ module avail

---------------------------------------------------------------------------------- /opt/ohpc/pub/modulefiles ----------------------------------------------------------------------------------
   EasyBuild/4.7.2      cmake/3.24.2    imsl/1.0              intel/2024.0.2      os            prun/2.2           vasp-gnu/6.3.2          vasp-intel/6.4.0 (D)
   autotools            gnu12/12.2.0    intel/2023.1.0 (D)    libfabric/1.18.0    papi/6.0.0    ucx/1.14.0         vasp-gnu/6.4.0   (D)
   charliecloud/0.15    hwloc/2.9.0     intel/2023.2.3        ohpc                pmix/4.2.6    valgrind/3.20.0    vasp-intel/6.3.2

  Where:
   D:  Default Module

If the avail list is too long consider trying:

"module --default avail" or "ml -d av" to just list the default modules.
"module overview" or "ml ov" to display the number of modules for each name.

Use "module spider" to find all possible modules and extensions.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".
```

Si ahora cargamos un módulo, por ejemplo, `module load gnu12`, veremos más módulos disponibles cuando ejecutemos `module avail`.

```
[bruzzo@sol76 ~]$ module load gnu12
[bruzzo@sol76 ~]$ module avail

------------------------------------------------------------------------------- /opt/ohpc/pub/moduledeps/gnu12 --------------------------------------------------------------------------------
   R/4.2.1      hdf5/1.14.0          impi/2021.11    metis/5.1.0           mpich/3.4.3-ofi    openblas/0.3.21    pdtoolkit/3.25.1    py3-numpy/1.19.5    superlu/5.2.1
   gsl/2.7.1    impi/2021.9.0 (D)    likwid/5.2.2    mkl-intel/2023.1.0    mvapich2/2.3.7     openmpi4/4.1.5     plasma/21.8.29      scotch/6.0.6

---------------------------------------------------------------------------------- /opt/ohpc/pub/modulefiles ----------------------------------------------------------------------------------
   EasyBuild/4.7.2      cmake/3.24.2        imsl/1.0              intel/2024.0.2      os            prun/2.2           vasp-gnu/6.3.2          vasp-intel/6.4.0 (D)
   autotools            gnu12/12.2.0 (L)    intel/2023.1.0 (D)    libfabric/1.18.0    papi/6.0.0    ucx/1.14.0         vasp-gnu/6.4.0   (D)
   charliecloud/0.15    hwloc/2.9.0         intel/2023.2.3        ohpc                pmix/4.2.6    valgrind/3.20.0    vasp-intel/6.3.2
```

Esa sección de módulos tiene como header `/opt/ohpc/pub/moduledeps/gnu12` ya que son los módulos que dependen de gnu12 para poder ser cargados exitosamente.

## SOL76

### FreeFem++

FreeFem se encuentra disponible como módulo para ser utilizado.

Script de ejemplo:
```
#!/bin/bash

#SBATCH -N 1
#SBATCH --job-name <job_name>
#SBATCH --output <output_name>_%j.out
#SBATCH --error <stderr_name>_%j.err
#SBATCH -w <node_list>
#SBATCH --partition=<partition_name>

module load freefem
FreeFem++ -ng example.cc >> output.txt
```

### Julia

Recomendamos instalar Julia de forma local. 
La integración de Julia con corridas distribuidas no es trivial, [hay plugins que facilitan el uso](https://github.com/JuliaParallel/ClusterManagers.jl?tab=readme-ov-file#load-sharing-facility-lsf).


### PennyLane

Utilizar micromamba para generar un enviroment para instalar las pennylane y sus dependencias.

Si no tiene micromamba en su usuario, ir al paso [Instalar Micromamba](#instalar-micromamba). Si ya tiene micromamba instalado, ir al paso [Crear Enviroment](#crear-enviroment).

(instalar-micromamba)=
#### Instalar micromamba
Asegurarse de tener instalado `micromamba`.

Si no está instalado, instar `micromamba`en el directorio de usuario:

Instalar con:
```"${SHELL}" <(curl -L micro.mamba.pm/install.sh)```

Aceptar cada una de las opciones predeterminadas de instalación:

Resetear la shell (ejemplo bash):
``` source ~/.bashrc ```

(crear-enviroment)=
#### Crear enviroment

Copiar el siguiente archivo y guardarlo como `dependencies.yaml`

```
name: pennylane_env
channels:
  - conda-forge
dependencies:
  - python >=3.10,<3.11.0a0
  - numpy < 2.0.0
  - pennylane
```

Luego, correr el siguiente comando:

`micromamba create -f enviroment.yaml`

#### Enviar jobs al cluster

Para enviar jobs al cluster que utilicen PennyLane, utilizar un script de lanzamiento que siga la siguiente estructura:

```
#!/usr/bin/bash
#SBATCH ...
#SBATCH ...

eval "$(micromamba shell hook --shell bash)"
micromamba activate pennylane_env
python example.py
```
o alternativamente:

```
#!/usr/bin/bash
#SBATCH ...
#SBATCH ...

eval "$(micromamba shell hook --shell bash)"
micromamba activate pennylane_env
python example.py
micromamba run -n pennylane_env python example.py
```
:::{note}
Se puede activar el enviroment en el front-end para hacer tests rápidos, pero los trabajos más demandantes deben ser enviados via slurm.
:::