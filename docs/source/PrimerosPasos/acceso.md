---
og:title: Conexión a los clusters
---

# 🔒 Conexión a los clusters

## Solicitud de cuentas

Actualmente las cuentas no están abiertas a personal que no forme parte del DFMC o sean colaboradores de alguna de sus líneas de investigación. La solicitud debe realizarse en persona en la oficina FRED (A-004).


## Acceso dentro del TANDAR 

El mecanismo utilizado para conectarse a los clusters es _Secure SHell_ o SSH. La mayoría de las distribuciones de Linux vienen con un cliente de SSH instalado. [PuTTY](https://www.putty.org/) es un cliente de SSH popular para usuarios de Windows.

:::{note}
Se asume que el sistema operativo del usuario es un sistema basado en Unix (Linux, macOS). Las instrucciones en Windows difieren significativamente.
:::

Si la PC se encuentra en la red interna del TANDAR, es posible acceder de forma directa con su nombre de usuario y  el nombre del cluster. Por ejemplo, para acceder a `sol67` con nombre de usuario `user` [^1] debe ejecutarse en una terminal

```bash
$ ssh user@sol67
```

Luego de esto se pide la contraseña con un prompt (sin indicación de los caracteres ingresados), y luego del acceso ya se puede usar la terminal del cluster normalmente. Para salir puede ejecutarse el comando `exit` o el atajo `CTRL+D`.

### Uso de aplicaciones gráficas

Algunas aplicaciones pueden requerir mostrar gráficos en la computadora local desde el cluster. Para múltiples aplicaciones esto solo es posible pasando argumentos adicionales a `ssh`:

```bash
$ ssh -CY user@sol67
```

## Acceso fuera del TANDAR

Para conectarse a los clusters desde fuera del TANDAR es necesario hacerlo mediante un _gateway_, un servidor dentro de la red TANDAR al que se puede conectar desde fuera y que tiene acceso directo a los clusters.  

Es necesario tener una cuenta en el _gateway_, que puede solicitarse también en la oficina FRED (A-004). Para acceder también se utiliza SSH:

```bash
$ ssh usergw@portal.tandar.cnea.gov.ar
```
El nombre de usuario de este «portal» puede diferir del nombre de usuario en el cluster. 
Es probable que en el primer intento de login falle con el siguiente mensaje
> _Unable to negotiate with 168.96.66.6 port 22: no matching key exchange method found. Their offer: ..._

Esto se soluciona agregando un argumento adicional a `ssh`:

```bash
$ ssh -oKexAlgorithms=+diffie-hellman-group-exchange-sha1 usergw@portal.tandar.cnea.gov.ar
```

o modificando la configuración en `$HOME/.ssh/config` para poder usar la versión «corta» del comando.

Algunos errores adicionales posibles son:

1. Que el servidor rechace el acceso. Esto ocurre porque el control de acceso se realiza mediante la IP de origen y, si bien los proveedores locales estan mayoritariamente habilitados, podría ocurrir que el proovedor específico o un grupo de sus IPs no lo esté. Esto puede pasar especialmente con intentos lejanos geográficamente del CAC (por ejemplo, es probable que una conexión desde tanto Salta como Bogotá sea rechazada inicialmente). Típicamente este problema puede identificarse cuando la conexión es cortada inmediatamente por el servidor.
2. Que por alguna razón el firewall de la red local desde donde se está conectando esté bloqueando conexiones de SSH.
3. Que la DNS del TANDAR no se encuentre activa y sea necesario loguearse a partir de la IP. El mensaje de error es similar al de la causa 1, pero no persiste al ejecutar el comando con la IP del portal: `ssh usergw@168.96.66.6`.

En caso de no poder conectarse, comuníquese con la administración a través de Discord o mail.

Es posible conectarse directamente al cluster con un único comando mediante

```bash
$ ssh -J usergw@portal.tandar.cnea.gov.ar user@sol67
```

donde `-J` indica que «saltamos» a través del portal para conectarnos a sol67.

## Intercambio de archivos
Es posible intercambiar archivos mediante su administrador de archivos. Por ejemplo, en Ubuntu o Fedora el navegador de archivos tiene una opción para poder conectar directamente con el prefijo `ssh://sol67` y luego una autenticación.

Sin embargo, este tipo de mecanismos depende fuertemente del sistema operativo particular que se use como cliente. Una alternativa es sincronizar a un servidor intermedio como GitHub, otra alternativa es mediante SFTP (_Secure File Transfer Protocol_). 

Para intercambiar archivos mediante SFTP se usa una sintaxis prácticamente idéntica a la de SSH. Por ejemplo, 

```bash
$ sftp -J usergw@portal.tandar.cnea.gov.ar user@sol67 
Connected to sol67.
sftp>
```

La consola en la que entramos tiene comandos básicos para movernos sobre el servidor remoto (`ls`, `cd`, `pwd`, etc.) y sobre el cliente local(`lls`, `lcd`, `lpwd`, etc.). Con estos comandos podemos buscar archivos o carpetas para transferir y el lugar en donde desamos enviarlos o recibirlos. Para descargar archivos se usa el comando `get` y `put` para subirlos (con `-r` en el caso de que se trate de un directorio). Por ejemplo, para descargar un archivo y una carpeta:

```bash
$ sftp user@sol67
Connected to sol67.
sftp> ls
CLOCK
Cu-fcc
sftp> lls
anaconda.bashrc
Audio
sftp> get CLOCK
Fetching /home/user/CLOCK to CLOCK
CLOCK                                                       100%   31     0.4KB/s   00:00    
get -r Cu-fcc
Fetching /home/user/Cu-fcc/ to Cu-fcc
Retrieving /home/user/Cu-fcc
Cu.e18                                                      100% 3291    43.6KB/s   00:00    
nodo.dat                                                    100%  192     1.8KB/s   00:00    
OUTCAR                                                      100%   41KB 461.1KB/s   00:00    
Cu.e19                                                      100% 2572   266.4KB/s   00:00    
WAVECAR                                                     100%  798KB   3.4MB/s   00:00    
OUTCAR_sol67-old                                            100%   56KB   4.8MB/s   00:00    
DOSCAR                                                      100%   11KB   1.2MB/s   00:00    
Cu.e20                                                      100%   16KB   2.0MB/s   00:00    
run-Cu.sh                                                   100%  300    29.4KB/s   00:00    
POTCAR                                                      100%  225KB   6.8MB/s   00:00    
PCDAT                                                       100%  234   127.5KB/s   00:00    
vasprun.xml                                                 100%   39KB   5.0MB/s   00:00    
XDATCAR                                                     100%  247   119.3KB/s   00:00    
CONTCAR                                                     100%  402   184.8KB/s   00:00    
IBZKPT                                                      100%  807   525.5KB/s   00:00    
CHGCAR                                                      100% 3124KB  11.1MB/s   00:00    
OSZICAR                                                     100%  419   196.9KB/s   00:00    
Cu.e22                                                      100%   16KB   4.6MB/s   00:00    
POSCAR                                                      100%   87    18.6KB/s   00:00    
Cu.e21                                                      100%   16KB   4.5MB/s   00:00    
output                                                      100%   10KB   3.8MB/s   00:00    
CHG                                                         100% 2075KB  10.2MB/s   00:00    
INCAR                                                       100%   95    52.9KB/s   00:00    
Cu-fcc.tgz                                                  100%   95KB   3.8MB/s   00:00    
KPOINTS                                                     100%   43    22.0KB/s   00:00    
EIGENVAL                                                    100% 4153     1.9MB/s   00:00  
```

Para subir archivos el proceso es similar:

```bash
sftp> put Audio/adams-hitchhiker-01.mp3 
Uploading Audio/adams-hitchhiker-01.mp3 to /home/user/adams-hitchhiker-01.mp3
adams-hitchhiker-01.mp3                                     100%   25MB  11.0MB/s   00:02
```

Los archivos son en principio copiados y no eliminados, pero se debe tener cuidado de no sobreescribir archivos.

## Autenticación sin contraseña
Es posible usar un método de autenticación que no requiere contraseña (en principio). Este método es el de _autenticación por clave pública-privada_. Esto permite conectarse sin usar contraseña. El método de autenticación genera en la computadora que va a comportarse como cliente una clave pública que puede compartirse con todos los equipos a los que se vaya a conectar, y una clave privada que debe permanecer en la computadora cliente. 

:::{warning}
Los archivos generados para la clave pública y privada suelen tener nombres muy parecidos. *Nunca* se debe compartir la clave privada. La clave pública frecuentemente va a tener el sufijo `.pub` que permite distinguirla de la privada. Si no funciona la autenticación con el archivo de ese sufijo no se debe intentar compartir la clave privada.
:::

Para generar un par de claves en una computadora con `ssh` instalado, se ejecuta

```bash
$ ssh-keygen -a 100 -t ed25519 -f ~/.ssh/id_ed25519 
```
(ed25519 es el nombre del algoritmo criptográfico utilizado)

A continuación se pide una *passphrase* opcional. La *passphrase* no es necesaria pero añade seguridad, dado que cualquier persona con acceso a su computadora puede loguearse en el cluster si no hay *passphrase*. La clave privada generada va a ser `~/.ssh/id_ed25519` y la pública `~/.ssh/id_ed25519.pub`.

Para poder loguearse en el cluster con esta autenticación, se debe copiar el contenido de `~/.ssh/id_ed25519.pub`, que tiene la siguiente forma 

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDVfypnCNc/ufTwhnO3qMTE/a5O4QDjJ6C7OeWYb5Yy6 user@host
```
Una vez copiado el contenido, se debe ingresar en el usuario del cluster y pegarlo en el archivo `~/.ssh/authorized_keys`. Es posible añadir múltiples claves para poder acceder desde distintos equipos. Una vez guardado este archivo ya debería ser posible loguearse remotamente sin contraseña.


[^1]: Naturalmente debe reemplazarse `user` con su correspondiente nombre de usuario.
