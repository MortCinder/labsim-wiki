---
og:title: Envío de trabajos
---

# 🚧 Envío de trabajos

## Slurm
Slurm es una sistema de _job scheduling_ para clusters de cómputo. Es responsable de asignar los recursos (nodos de cómputo) a los usuarios. Se encarga de arbitrar entre los distintos trabajos que se encuentran pendientes en la cola (queue) dependiendo de los recursos que estos requieran.

### Comandos básicos

`sbatch` se usa para enviar un _job script_ que encolará un job en la queue. El script puede contener comandos srun para lanzar tareas paralelas.

`scancel` se usa para cancelar un trabajo pendiente o en ejecución. 

`sinfo` reporta el estado de las particiones y los nodos manejados por Slurm. Tiene una gran variedad de opciones de filtrado, ordenamiento y formateo. 

`squeue` reporta el estado de los trabajos. Tiene gran variedad de opciones de filtrado, ordenamiento y formateo. Reporta los trabajos corriendo (running) en orden de prioridad y luego los trabajos pendientes en orden de prioridad.

```
squeue # todos
squeue -A account   # por cuenta
squeue -u usuario   # por usuario
```

:::{note}
Cada comando tiene su propia man page, por ejemplo, `man sbatch`. El comando `--help` también provee un sumario de opciones. Notar que los comandos son _case sensitive_.
:::

### Slurm Job Scripts
Se aconseja utilizar scripts para automatizar, documentar, y replicar correctamente los trabajos que enviamos al cluster via Slurm. 

Las directivas de Slurm comienzan con `#SBATCH`. 
`sbatch` deja dejará de procesar directivas #SBATCH al llegar a la primer linea que no sea ni un comentario ni una linea en blanco.

Luego, comenzará a ejecutar los comandos del script.

#### Ejemplo

```
 #!/bin/bash
#SBATCH --job-name=matrix_multiplication
#SBATCH --output=matrix_multiplication_%.out
#SBATCH --error=matrix_multiplication_%.err
#SBATCH --ntasks-per-node=1
#SBATCH --partition=debug

# Module loading
module purge
module load gnu12

# Compile program
gfortran -o matrix_multiplication matrix_multiplication.f90

# Execute the compiled program
./matrix_multiplication
```

Para enviar este trabajo a slurm, use el siguiente comando:

`sbatch <script_name>`

Puede monitorear la posición del trabajo en la cola con el comando `squeue`.

Una vez que el programa comienza a ejecutarse, va a poder ver el output en los archivos indicados por `#SBATCH --output` y `#SBATCH --error`.

:::{note}
Para ver todas las opciones para configurar los trabajos que ofrece SLURM, [vea la documentación de SLURM](
https://slurm.schedmd.com/sbatch.html#SECTION_OPTIONS).
:::

### Hold/Release jobs

Para postergar la asignación de un job en estado PENDING: 

```
scontrol hold 5975   # por job ID
scontrol release 5975   # permitir que sea programado para comenzar. 
```

### Fair Share

Al momento de asignar un recurso libre, el scheduler determinará la prioridad de los jobs en base a una cuota de uso que tiene cada usuario. 
Esta cuota dependerá de un valor inicial de _shares_ que tiene cada usuario, las cuotas asignadas a su respectiva account, la cantidad de horas/cómputo que el usuario ha consumido en el último período de tiempo, y la partición seleccionada para el job.

Se puede monitorear con el comando `sshare`.

```
sshare -a           # todos los usuarios
sshare -u user      # share del usuario
sshare -A account   # share de la account
```

:::{note}
Para más información sobre cómo funciona el algoritmo de Fair Share, [vea la documentación de SLURM](https://slurm.schedmd.com/fair_tree.html#enduser).
:::

## OpenPBS

PBS es otro sistema de scheduling usado por la mayoría de los clusters del LABSIM.
