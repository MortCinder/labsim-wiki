# Monitoreo

Infinidad de problemas pueden surgir de administrar un cluster. Es también
importante mantener cuenta del rendimiento del sistema para identificar posibles 
ineficiencias y observar estadísticas de uso en el largo plazo, y analizar los
límites de performance en el corto plazo. Por esto, es una práctica estándar el
monitoreo/monitoring de un sistema de nodos, de manera individual y en su totalidad.

El problema de monitoreo se divide en tres partes: 
1. Una parte conceptual que consiste en decidir qué métricas se van a tomar, 
en qué intervalo y resolución temporal, y cómo se va a organizar y almacenar
esa información para poder visualizarla.
2. La recolección y transmisión de esas métricas (de manera local).
3. El agregado de los datos recolectados y la visualización.

El primer problema es quizás el más importante y el menos enfatizado. Monitorear
cosas inútiles en un sistema dado puede desviar de problemas reales. Tener una 
refresh rate de 5 segundos en un sistema que requiere de alta performance y puede
llegar a tener problemas no detectables en esa escala temporal es extremadamente
peligroso y puede traer problemas imposibles de diagnosticar. Almacenar todas las
métricas en existencia en tiempo real y permanentemente, en un sistema cuyo único 
rol es almacenar archivos es costoso e ineficiente. 

Por ahora estoy teniendo demasiados problemas implementando una buena solución de
los puntos 2 y 3 cómo para poder hacer un análisis más riguroso, pero [Brendan
Gregg](https://www.brendangregg.com/) tiene infinidad de recursos para profundizar
más en estos temas y [este artículo](https://www.admin-magazine.com/HPC/Articles/HPC-Monitoring-What-Should-You-Monitor)
quizás ayuda a bajarlo un poco más a Tierra.

## Herramientas de monitoreo individual

A pesar de que este artículo se refiere más a herramientas de monitoreo globales
y automatizadas, también es extremadamente útil poder monitorear a nivel individual
(i.e. logueado como usuario via ssh). Algunas de estas opciones son:

* `htop`: una variación de `top` un poco más bonita. 
* `nmon`: herramienta con más opciones por separado que `htop`.
* `bashtop`: otra variante más
* `sysstat`: conjunto de herramientas útiles, entre ellas para ver I/O
* `perf` + FlameGraph: [véase este sitio](https://www.brendangregg.com/linuxperf.html)
* `ipmitoool`: permite monitorear magnitudes físicas del cluster (temperatura,
fan speed, etc).

A pesar de que estas herramientas son poderosas y permiten adquirir una cantidad
razonable de datos dentro de cada nodo, no son paralelizables en el sentido de que
no es trivialmente posible obtener de manera conjunta los datos para todos los nodos.
En casos como el de `perf` hasta es probable que no tenga sentido hacerlo dado que se
usa para problemas muy específicos de application performance.

Buscamos entonces una manera de adquirir métricas de los nodos y una forma de visualizarlas.

## Prometheus + Grafana

Una de las opciones más usadas en monitoreo de grandes cantidades de nodos es usar
[Prometheus](https://prometheus.io/) como conector y [Grafana](https://grafana.com)
como visualizador. A pesar de que alcancé cierto grado de funcionamiento con este
stack, encontré que Grafana:
1. Usa una variedad de nombres enorme para referirse a sus métricas, lo que a menudo
rompe los gráficos de dashboards importados.
2. Usa su propio pseudolenguaje para armar estas métricas. Cuando el lenguaje cambia,
los gráficos se rompen.
3. Tiene una learning curve demasiado grande para crear un dashboard funcional de HPC.

Por supuesto, es posible aprender el lenguaje y armar un dashboard a fuerza de calibrar
cada uno hasta obtener un resultado aceptable. Sin embargo, esto es un nivel de complejidad
y customización que no necesitamos. Lo que necesitamos es un monitor que:

1. Muestre las métricas básicas del sistema de manera conjunta (entre todos los nodos).
2. Permita profundizar en cada nodo y distintas escalas temporales.

Por lo pronto, Grafana queda descartada. No es muy conveniente tener un monitor que requiera
mantenimiento propio ad infinitum.

## Netdata

(En observación, difícil de centralizar los nodos) 
