# Administración de software

La administración de software es algo fuertemente importante en un entorno de HPC. Esencialmente,
como se menciona en la sección de [Provisioning](/Fundamentos/Provisioning), es imposible mantener copias 
individuales del software en cada nodo. Por eso, es necesario una solución que los distribuya.
Adicionalmente, dada la abundancia de bibliotecas especializadas y compiladores, las versiones
estandarizadas provistas por los package managers pueden resultar subóptimas. Por último, esa
misma abundancia genera posibles conflictos entre versiones en conflicto del mismo software
(Por ejemplo, distintas versiones de un compilador como `gcc` usarían el mismo ejecutable si
no hay un mecanismo para diferenciarlas).

## Distribución del software a los nodos
Una de los aspectos más importantes de la administración de software es mantener copias idénticas
del mismo stack de software en los nodos. Con algún mecanismo de provisioning, el approach normal
es simplemente modificar la imagen del sistema operativo que se provisiona a los nodos.
En Rocks, por ejemplo, se provee de una imagen básica para los nodos de cómputo y cualquier paquete
adicional [debe ser agregado como un RPM y puesto en un XML](http://central-7-0-x86-64.rocksclusters.org/roll-documentation/base/7.0/customization-adding-packages.html)
para poder ser comprimido en la misma imagen que va a los nodos. Dado que esto es muy incómodo de 
realizar para todos los paquetes, y a veces se requieren configuraciones adicionales, otra forma 
de manejar la instalación de software es usando Rolls, que permiten simplificar la instalación
(distribuyendo el software y los config files en los nodos). La desventaja de este approach 
está en que cada roll requiere de un mantenimiento propio, no soportado por Rocks. Algunos de los 
problemas de instalación para los clusters de prueba con Rocks 7 tenían que ver con este mismo 
aspecto.
Por otro lado, programas de provisioning como Warewulf proveen un `chroot`: esto es, un 
árbol con su propio directorio `/`, `/home`, etc. todo dentro de un subdirectorio del sistema.
Este árbol actúa como un sistema propio, con la posibilidad de instalar paquetes, cambiar 
configuraciones de red, etc. Entonces, la forma de instalar software en los nodos consiste en 
instalar software en el chroot con (usando yum)

```
yum install --install-root=CHROOT_DIR package
```
desde el environment normal del frontend o
 
```
chroot CHROOT_DIR
yum install package
```
si yum está instalado en el chroot. A su vez, es posible modificar config files dentro de ese
chroot y que eso se distribuya también a los nodos. Las dos desventajas principales de este 
método son que

1. El proceso de armar la imagen para distribuir en los nodos tarda bastante y se aplica después
de un reboot.
2. Al ser el chroot y el environment en el frontend similares, es muy fácil confundir uno con 
otro.

La primera es común con Rocks, resta ver si opciones como [xCAT](https://xcat.org/) permiten 
reducir el tiempo de compresión y almacenamiento de la imagen.

## Compilación de software especializado - Spack / EasyBuild

El nivel de especialización, diferencias en versiones, y grados de optimización en la compilación
de HPC hace que usar simplemente los paquetes provistos por un sistema operativo ordinario resulte 
insuficiente para mantener un sistema de HPC. Esto hace necesario algún mecanismo parecido a `pip`
o `cargo`: una especie de package manager paralelo que permita compilar el software en una versión
arbitraria, con distintas opciones de compilador o compilación, y que resulte agnóstico al proceso 
de compilación en particular (lenguaje, herramientas de compilación, etcétera). 

[Spack](https://spack.readthedocs.io/en/latest/) provee una solución a este problema. Esencialmente
se comporta como un package manager y permite instalar paquetes con distintas opciones de compilación
y versión, dentro de un mismo environment separado del resto del sistema. Es además posible agregar
software instalable mediante spack creando una "receta" a partir de instrucciones de compilación que
ya hayan probado ser efectivas. Esto se suma como pull request a spack y pasa a estar disponible para
toda persona que lo use.

[EasyBuild](https://easybuild.io/) es otra alternativa más centrada en la compilación, que también
permite instalar software compilado a partir de "recetas". El enfoque parece ser distinto pero el 
nivel de satisfacción en sus usuarios y userbase mayor a Spack hacen que sea importante investigarlo
en mayor detalle. Es notable que Spack e EasyBuild no son mutuamente excluyentes y que puede ser un
buen ambiente el usar ambos en simultáneo.

Con estas dos alternativas, es posible instalar distintas versiones del mismo software. La pregunta
sería ¿cómo el usuario elige usar distintas versiones del software en distintas combinaciones?


## Administración de versiones - Environment Modules / Lmod

Típicamente, cuando se usa un programa como `gcc`, se usa una única versión dentro del PATH:

```bash
$ which gcc
/usr/bin/gcc
```

Para manejar distintas versiones de un mismo programa, es necesario apuntar el PATH hacia otro lado
sólo para ese programa e incluir alguna manera de categorizar las distintas versiones.
Programas como [Environment Modules](http://modules.sourceforge.net/) y 
[Lmod](https://lmod.readthedocs.io/en/latest/) cumplen esta función. Un módulo es una versión específica
del software (un programa, conjunto de programas o bibliotecas) que ya se sabe que funciona y que puede 
"cargarse" desde cualquier consola del sistema. Por ejemplo,

```bash
$ which gcc
/usr/bin/gcc
$ module load gnu9
$ which gcc
/opt/ohpc/pub/compiler/gcc/9.4.0/bin/gcc
```

Programas como Spack permiten crear automáticamente `modulefiles`, que son los archivos que definen cómo
un módulo está compuesto y sus características.
