# Bibliotecas Especializadas

## MPI

Esta sección va a quedar pendiente para cuando tenga una mejor idea de las implementaciones
particulares. Por lo pronto, [MPI](https://www.mpi-forum.org/) es el estándar principal 
usado para paralelización. Tiene muchas implementaciones, entre ellas:

* [OpenMPI/OMPI](http://www.open-mpi.org/)
* [MPICH](https://mpich.org/)
* [MVAPICH](http://mvapich.cse.ohio-state.edu/)

## OpenMP

[OpenMP](https://www.openmp.org/) es una API que permite realizar multiprocesamiento con 
memoria compartida. Es más práctica de implementar MPI desde cero porque permite paralelizar
un programa no paralelo agregando directivas sin necesitar modificaciones extensas del 
código original.

## BLAS

[Basic Linear Algebra Subprograms (BLAS)](http://www.netlib.org/blas/) es una biblioteca de
Fortran con soporte para operaciones básicas de Álgebra Lineal (adición de vectores, 
producto interno, productos escalares, productos internos, productos vectoriales, 
multiplicación matricial, etc.) optimizadas para hacerlas de la manera más eficiente. 
La implementación open-source más usual de BLAS es [OpenBLAS](https://www.openblas.net/), que
por ejemplo se usa como base para NumPy en Python.

## LAPACK

Otro paquete de Álgebra Lineal vinculado a BLAS es [LAPACK](https://www.netlib.org/lapack/), 
que contiene algoritmos de resolución de ecuaciones lineales, cuadrados mínimos, SVD, autovalores
y autovectores, factorizaciones matriciales, etc.

[ScaLAPACK](http://www.netlib.org/scalapack/) es un superset de LAPACK con soporte para sistemas
de memoria distribuida.

## FFTW

[FFTW](https://www.fftw.org/) es una biblioteca de alto rendimiento que implementa uno de los
algoritmos más importantes de todos los tiempos: la [FFT](https://www.wikiwand.com/en/FFT) y
algoritmos vinculados(DFTs, etc.), que hace transformadas de Fourier en tiempo $`O(n \log(n) )`$.

Al ser usada extensivamente, es importante entonces tener una buena implementación, y en distintas
versiones.
