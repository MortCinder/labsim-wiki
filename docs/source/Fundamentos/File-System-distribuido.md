# File System Distribuido

Del mismo modo en el que en la sección de [Provisioning](/Fundamentos/Provisioning) plantea la imposibilidad de mantener
un sistema de HPC que requiera un mantenimiento individual de cada nodo en cuestión de software, es igualmente imposible
mantener un sistema de HPC en el que cada nodo tenga un sistema de archivos independiente, por lo menos en lo que 
respecta al userspace.

Adicionalmente, la performance de un sistema en el que todos los nodos realizan cómputos sobre copias individuales de los
mismos datos sería notablemente pobre (por lo menos si el esquema es ingenuo y no usa alguna estrategia coordinada). En 
ese aspecto también existen sistemas distribuidos orientados a mejorar la performance del I/O a fin de reducir 
bottlenecks que pueda causar su lentitud.

Algunos filesystems distribuidos de alto rendimiento son
* [Lustre](https://www.lustre.org/)
* [BeeGFS](https://www.beegfs.io/c/)
* [Ceph](https://docs.ceph.com/en/pacific/)

Tienen distintos niveles de complejidad y dificultad en su setup. Típicamente involucran generar uno o múltiples 
servidores e instalar un cliente en los nodos, además de otros pasos. En el laboratorio no se ha implementado hasta 
ahora ningún sistema de este tipo, aunque sí en el centro de cómputo de CNEA (donde usan Lustre). Es una buena idea para 
el futuro, siempre y cuando no interrumpa demasiado el flujo de trabajo. Además, si el requerimiento sobre los servidores
a nivel hardware no es muy excesivo entonces quizás sería posible reciclar nodos viejos para que cumplan esa función.
Tanto OpenHPC como Rocks no ofrecen mecanismos para instalar servidores de estos sistemas (OpenHPC sí para los clientes).

Otra opción, y este es el mecanismo menos específico para HPC, es usar *NFS* (*Network File System*). Un servidor NFS 
esencialmente hace un broadcast de uno o más directorios a través de la red, y los clientes esencialmente los montan.
A pesar de que este mecanismo no está optimizado para alto rendimiento, sí permite centralizar el almacenamiento de los
usuarios en servidores con mayor resiliencia a fallas y disponibles constantemente (a diferencia de nodos que pueden
ponerse de baja por múltiples razones). Lo que se usa en el laboratorio, por ahora, es NFS.

A pesar de que con NFS es posible montar directorios en los nodos o frontend editando `/etc/fstab` y configurando los 
mounts correspondientes, también es posible configurar el montado automático de ciertos directorios a través del uso 
de `autofs`/`automount`. Esto se regula a través de la configuración correcta de `/etc/auto.home` y `/etc/auto.master`
en el caso de los directorios montados sobre `/home`. Para más detalles en cuanto a configuración es útil revisar las
ya existentes en sol67/sol117, y la 
[guía oficial de RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/ch-nfs),
que está excelentemente documentada.
