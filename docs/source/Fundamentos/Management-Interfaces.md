# Management Interfaces

La administración de nodos remotamente requiere de poder realizar un número de 
operaciones que requieren de una cierta cercanía al *bare metal*. Algunos equipos
proveen de una interfaz (o varias) para poder:

* Apagar y prender el equipo remotamente.
* Instalar un sistema operativo rápidamente/de manera remota.
* Monitorear el estado del hardware.
* Monitorear la pantalla del equipo.
* Actualizaciones de firwmare.
* Extra goodies provistos por el manufacturador.

Las interfaces muchas veces pueden accederse en una dirección IP propia, 
independiente de la IP propia del sistema, a través del mismo cable de
red por donde pasa el resto del tráfico. Otras veces puede ser que el 
puerto administrativo sea único y sólo accesible a través de sumar cables
de red extra a la red.

## IPMI

IPMI es la interfaz más usada para realizar varias de las operaciones más requeridas.
[Este otro artículo entra más en detalle](IPMI) sobre cómo se configura a nivel 
red. 

## iDRAC

iDRAC es la interfaz provista por los nodos de Dell. Típicamente se encuentra 
disponible a través de una interfaz web o durante el proceso de booteo apretando
CTRL+E. Aquí se puede configurar la IP a ser usada por IPMI.

## iLO

iLO es la interfaz provista por los nodos HP. Es bastante parecida a iDRAC pero 
a veces puede resultar más molesta de acceder dependiendo de si el nodo requiere
usar el puerto de administración o no.
