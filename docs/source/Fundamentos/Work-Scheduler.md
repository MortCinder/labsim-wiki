# Work Scheduler

El administrador de trabajos y el sistema de colas son dos conceptos íntimamente relacionados y fundamentales para un
sistema de HPC. El sistema de colas es un software que recibe "trabajos" por parte del usuario (que incluyen 
instrucciones sobre qué bibliotecas usar, en qué tipo de nodos se deben ejecutar, etc.) y los pone en una cola que 
determina el orden en el que se ejecutan. Estas colas pueden tener distintas propiedades: Distintos límites de tiempo, 
distinta cantidad de recursos asignados, distintos grupos que tienen permitido utilizarlas, etc.

Al ser los clusters un recurso compartido, no siempre es preferible que el sistema de acceso a estas colas sea "first
come, first serve". Ahí es donde entra en juego el Work Scheduler o administrador de trabajos, que regula la política 
con la que se usa el sistema de colas. Esta política permite implementar distintos sistemas de "fair share", en donde 
cada usuario tiene derecho a usar el cluster y ninguno tiene el derecho de acaparar todos los recursos. 

Los sistemas de colas actuales son esencialmente Slurm y sistemas basados en PBS. En el laboratorio se usa históricamente
Torque (un derivado de PBS que ahora es PBSPro) y Maui como Work Scheduler. Para la migración a OpenHPC la idea es usar
OpenPBS para que las diferencias interfieran lo menos posible con el stack anterior. A la fecha todavía no se investigó
cómo se hace el work scheduling en OpenPBS, es posible que tenga una solución incorporada.

Operaciones típicas son:

Ver el estado de una cola

```
qstat
```
poner un nodo offline

```
pbsnodes -o nodo
```
(offline significa que el nodo no acepta más trabajos y puede ponerse fuera de línea para mantenimiento)

Se pueden ver los nodos con problemas con 

```
pbsnodes -l
```

O todos los nodos con sus características con

```
pbsnodes -a
```

Para operaciones más específicas, la [documentación de Torque](http://docs.adaptivecomputing.com/torque/6-0-1/help.htm)
y la User Guide de PBSPro son muy útiles, aunque un poco extensas.
