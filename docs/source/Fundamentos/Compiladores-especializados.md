# Compiladores Especializados

Compiladores usuales como `gcc`, `g++` o `gfortran` normalmente son suficientes
para crear programas de buen rendimiento y relativamente portables dentro de una
misma arquitectura. Sin embargo, cuando el interés está en producir programas más 
rápidos, es útil buscar compiladores idóneos para el hardware disponible.

## Intel OneAPI
En el laboratorio se usan en su totalidad (o casi) procesadores Intel. Lo bueno 
de esto es que Intel tiene su propia suite de compiladores optimizados para sus
propios procesadores. Anteriormente Intel Parallel Studio y Composer Studio, 
recientemente el nombre cambió a Intel OneAPI.

Críticamente, las versiones de RHEL/CentOS soportadas por Intel son 7 
(parcialmente) y 8. Aunque la versión nueva es gratuita pero no open source, 
anteriormente sí era paga y se dispone en el laboratorio de la versión de 2013
instalable en Rocks mediante un roll del centro de supercómputo de San Diego
(que requirió modificaciones hechas por Darío para poder funcionar 
apropiadamente, los cambios están plasmados en un repo de GitHub privado
al que se puede ceder acceso). Históricamente Intel oscila entre un modelo
pago y gratuito y no hay garantía de que el acceso siga siendo libre en el
futuro.

OneAPI tiene la desventaja de que no es estándar instalar compiladores por separado
(siendo por ejemplo `ifort` uno importante dentro del uso en el laboratorio). Esto 
implica que es más práctico instalar directamente los "Toolkits", que suelen pesar 
bastante (~30 GB). Los toolkits necesarios por ahora son Base y HPC, instalables
mediante 

```bash
$ wget https://registrationcenter-download.intel.com/akdlm/irc_nas/18487/l_BaseKit_p_2022.1.2.146.sh

$ sudo sh ./l_BaseKit_p_2022.1.2.146.sh

$ wget https://registrationcenter-download.intel.com/akdlm/irc_nas/18479/l_HPCKit_p_2022.1.2.117.sh

$ sudo sh ./l_HPCKit_p_2022.1.2.117.sh
```
(Seguramente esto se vuelva rápidamente obsoleto, revisar la página de descargas de
OneAPI)

o alternativamente mediante el repositorio de OpenHPC. Es crucial que si se instala en una Desktop
esté `gcc` instalado y actualizado, o si no `ifort` puede no funcionar.

Típicamente, en una instalación común (no la de OpenHPC) es necesario correr un script `setvars.sh`
para exportar las variables de entorno y agregar el PATH correcto. En ambos tipos de instalación, 
también normalmente hay módulos que es posible cargar para acceder a los compiladores (con nombre
de módulo `intel` y módulos disponibles luego de cargar ese).

Algunas de las opciones que estuvimos viendo para compilar son `-static-intel` (usa linkeo estático
para las bibliotecas de intel) y `static-libgcc` (ídem para libgcc). Por ahora parece haber algunos
problemas con `-static` a secas, que no puede cargar algunas bibliotecas básicas de C.
