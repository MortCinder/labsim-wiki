# Infiniband

[Infiniband](https://www.wikiwand.com/en/InfiniBand) es una tecnología desarrollada por Mellanox (ahora parte de
NVIDIA). Puede alcanzar anchos de banda considerablemente mayores a Ethernet y, crucialmente, latencias mucho más bajas.
Hay distintos tipos de velocidades y protocolos soportados. Algo importante es que, de manera similar a los cables CAT
de Ethernet, hay distintas generaciones de Infiniband típicamente identificadas como ConnectX [1-7]. Actualmente, 
las interfaces correspondientes a ConnectX menor o igual a 3 están quedando obsoletas en End of Life (EOL) y perdiendo 
soporte. Las redes de Infiniband presentes en el laboratorio son ConnectX (sol117) y ConnectX 3(sol67).

Nota: Con la adquisición de Mellanox por parte de NVIDIA todavía no se migraron los foros de Mellanox 
(community.mellanox.com) y sólo están disponibles mediante la caché de Google.

Hay dos instalaciones de Infiniband: OFED (y paquetes asociados, soporte genérico) y MLNX OFED (soporte de Mellanox).
Hasta ahora no pude hacer funcionar el soporte genérico por lo viejo de las placas, y sí MLNX OFED; que ya se usaba 
en el laboratorio. La instalación de MLNX OFED tiene múltiples opciones y posibles problemas, aunque está parcialmente 
documentado en la migración de sol117.

Algunos comandos útiles para Infiniband:

```bash
$ ibstat # Datos de la interfaz
$ ibnodes # Datos de los nodos conectados por Infiniband, incluido el switch
$ ibnetdiscover # Usado inicialmente para encontrar nodos en la red
$ ibnetdiag # Diagnóstico extensivo, dependiente en algunos casos de las últimas actualizaciones sobre los drivers
```

Es muy útil la [documentación oficial de Mellanox OFED](https://docs.nvidia.com/networking/m/view-rendered-page.action?abstractPageId=12013389)
(en la versión correspondiente), y en menor medida la 
[documentación de RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/part-infiniband_and_rdma_networking).
A su vez, dada la obsolescencia de versiones anteriores, son notables el [patch de Mellanox OFED en OpenHPC](https://github.com/viniciusferrao/mlnxofed-patch)
y los [drivers de ELRepo](https://www.mail-archive.com/elrepo@lists.elrepo.org/msg03895.html) para el caso de otras 
implementaciones. 
