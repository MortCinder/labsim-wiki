Wiki del LabSim
===========================================
Información útil para correr en los clusters del Laboratorio de Simulación, Modelado y Diseño Computacional del DFMC en CAC-CNEA. 

:::{warning}
Wiki en progreso!
:::

Contenidos
----------
Para quienes nunca usaron un cluster de cómputo, se recomienda seguir los siguientes primeros pasos.


```{toctree}
:caption: Primeros Pasos 
:maxdepth: 2

PrimerosPasos/acceso.md
PrimerosPasos/jobs.md
PrimerosPasos/modulos.md
```


Documentación admines 
---------------------
:::{warning}
Esta sección documenta algunos elementos necesarios para la administración de clusters, no para su uso. Usuarios entrando más allá de este punto: [_Caveat Emptor_](https://www.wikiwand.com/en/Caveat_emptor)! 
:::

[HPC](https://www.wikiwand.com/en/High-performance_computing) es un campo de la computación inabarcable para una humilde Wiki de administración como esta. Sin embargo, la implementación más usual de clusters de cómputo i.e. múltiples computadoras compartiendo recursos para alcanzar un mayor rendimiento conjunto, sí es algo que debe ser documentado mínimamente aunque sea desde un punto de vista práctico. En esta página de inicio voy a ir poniendo aspectos básicos que definen a un cluster, y después voy a documentar esos aspectos (y otras cosas) para cada stack de software actualmente en uso en el laboratorio: Rocks y OpenHPC.

:::{note}
En algunos casos se usa `$` para simbolizar la terminal aunque se deba estar logueado como root. Como regla general, hay que estar logueado como root a menos que se diga lo contrario.
:::
```{toctree}
:caption: Fundamentos de clusters de cómputo
:maxdepth: 1

Fundamentos/Administración-de-software.md
Fundamentos/Bibliotecas-especializadas.md
Fundamentos/Compiladores-especializados.md
Fundamentos/Estructura-del-cluster.md
Fundamentos/File-System-distribuido.md
Fundamentos/Infiniband.md
Fundamentos/Management-Interfaces.md
Fundamentos/Monitoreo.md
Fundamentos/Provisioning.md
Fundamentos/Sistema-Operativo.md
Fundamentos/Work-Scheduler.md
Fundamentos/IPMI.md
topologias/topologias.md
```

Administración
-----
```{toctree}
:caption: Administración
:maxdepth: 1
SLURM/slurm-admin.md
Profiling/vtune-drivers.md
```

OpenHPC
-------

```{toctree}
:caption: OpenHPC
:maxdepth: 1
OpenHPC/Acceso-a-internet-en-nodos.md
OpenHPC/Agregado-de-drivers.md
OpenHPC/Prometheus+Grafana.md
OpenHPC/Spack.md
OpenHPC/Warewulf.md
```

Rocks
-----

```{toctree}
:caption: Rocks
:maxdepth: 1
Rocks/Agregado-de-drivers.md
```

Benchmarks
----------

```{toctree}
:caption: Benchmarks
:maxdepth: 1
Benchmarks/Optimización-hibrida-(OpenMP-MPI)-de-VASP.md
```

Intel OneAPI
------------

```{toctree}
:caption: Intel OneAPI
:maxdepth: 1
OneAPI/parallel-random-number-generation.md
```

```{toctree}
:caption: ReadTheDocs Wiki
:maxdepth: 1
Wiki/intro-wiki.md
```
