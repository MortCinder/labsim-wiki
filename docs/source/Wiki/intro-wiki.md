# Agregar documentación

Pasos a seguir para poder agregar/editar contenido a esta documentación.

## Entorno de desarrollo

Archivos y herramientas necesarias para poder clonar y editar esta documentación.

### Repositorio

Clonar el repositorio de la wiki:

```console
git clone https://gitlab.com/MortCinder/labsim-wiki
```

### Virtual Enviroment

Construir un _virtual enviroment_ y activarlo.

```console
python -m venv env
source env/bin/activate
```

Instalar dependencias ubicadas en `requirements.txt`

:::{note}
Al instalar nuevas dependencias con pip, agregarlas a `requirements.txt`.
:::

```console
pip install -r requirements.txt
```

Para salir del enviroment, ejecutar el comando `deactivate`.

### Live HTML

La documentación se construye con [Sphinx](https://www.sphinx-doc.org). 

Para poder ver automáticamente los cambios hechos en la versión local de la documentación, construir con `sphinx-autobuild`.

```console
sphinx-autobuild docs/source docs/build/html
```

La versión local va a poder accederse en [http://127.0.0.1:8000](http://127.0.0.1:8000).

## Edición

Para la edición de las páginas, usamos MyST, una versión extendida del Markdown tradicional. 

Los archivos fuente se encuentran en el directorio `docs/source/`

Puede crear páginas nuevas o editar las páginas que se encuentran presentes en este directorio.

Para que aparezcan en el glosario de la página principal y en la sidebar, hay que agregarlas al `toctree` que se encuentra en `docs/source/index.md`.


## Publicar cambios

Una vez que termine de editar la página, es necesario pushear los cambios al repositorio.

Readthedocs automáticamente publica los cambios en la Wiki cuando identifica una nueva version de _latest_ en el repositorio.


