# Acceso a internet desde los nodos

Este archivo es, esencialmente, una recopilación de dos recursos que usé para hacer algo
por fuera de la instalación por "receta". Aunque la misma información puede sacarse 
de [este post en el grupo de OpenHPC](https://groups.io/g/OpenHPC-users/topic/5520996#2097), 
dejar ese link sería irresponsable de mi parte porque
1. Es vulnerable a link rot.
2. No explica el proceso.
3. ¡No es trivial de encontrar! (i.e. no es de los primeros resultados en un search engine).
4. Tener este archivo entre las primeras cosas de OpenHPC que documento aclara lo que viene 
a continuación de entrada, en lugar de dejarlo escondido en una receta de instalación.

Y esa aclaración es importante: la receta estándar de OpenHPC dice que para el funcionamiento
correcto de Warewulf (y asumo que xCAT en las otras recetas) firewalld tiene que estar 
totalmente deshabilitado. Pero lo que dicen algunos usuarios (y yo concuerdo) es que esto 
es simplemente para evitar documentar la configuración del firewall apropiadamente y ahorrarse
esos problemas.

Para una explicación teórica de cómo funciona el aspecto de redes, dejo la explicación de 
Roberto:

> Cuando queres que una subred (en este caso, la de los nodos, 10.0.0.0/24)
pueda acceder a la red "exterior" (y en particular, a internet) tenes
dos posibilidades: 1) routear 2) hacer NAT. En el caso 1) configuras una
maquina de la subred para que vincule ambas redes y habilitas la opcion
"foward", de modo que los paquetes puedan "atravesar" esa maquina CON
SU PROPIA IDENTIDAD (i.e. su nro IP) y acceder al exterior. La otra
posibilidad es habilitar el mecanismo NAT (Network Address Translation)
en la misma maquina que vincula ambas redes. En este caso las maquinas
internas acceden al exterior "emascarando" la comexion en un nro IP
de la red exterior (usualmente el de la maquina vinculante). En un
caso como este, en el que la red interna utiliza nros. IP privados,
siempre deberia usarse NAT (aunque se puede usar el forwarding con
ciertas limitaciones). Hay una diferencia fundamental entre anbos
metodos. En el caso 1) es posible acceder a las maquinas de la red
interna desde la red exterior, mientras que en el caso 2) quedan
completamente ocultas. es cierto que hay metodos para hacerlo -con
restricciones- pero se logra mediante otras tecnicas. La facilidad
de NAT en Linux se implementa en el kernel utilizando las iptables
(hay una tabla especial para ese proposito que justamente se llama
"nat"). La configuracion se hace con el mismo comando iptable, pero
es sumamente "unfriendly"... Todos los sistemas implementan algun
mecanismo para interactuar con las iptables que ademas permiten
guardar una configuracion que permanece a traves de una reboot.
Habria que ver como se hace eso en el sistema que estas usando.

Para configurar el frontend como gateway para todos los otros, se
modifica la configuración mediante Warewulf (como root):

```bash
wwsh node set \* -D <INTERFAZ INTERNA> --gateway=<IP FRONTEND>

wwsh node set \* -D <INTERFAZ INTERNA> –netmask=255.255.255.0

wwsh node print
```

En el frontend se habilita NAT con 

```bash
echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.conf

sysctl -p
```

Y a su vez debe configurarse el firewall con 

```bash
systemctl enable firewalld.service
systemctl start firewalld.service

## cluster_internal
firewall-cmd --permanent --new-zone=cluster_internal
firewall-cmd --permanent --change-zone=<INTERFAZ INTERNA> --zone=cluster_internal
firewall-cmd --reload

## Public services
firewall-cmd --zone=public  --permanent --add-service=ssh
firewall-cmd --zone=public  --permanent --add-service=http
firewall-cmd --zone=public  --permanent --add-service=https

## Port forwarding
firewall-cmd --zone=public --permanent --add-masquerade
firewall-cmd --zone=cluster_internal --permanent --add-masquerade
firewall-cmd --reload

firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -o INTERFAZ INTERNA -j MASQUERADE
firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i INTERFAZ INTERNA -o INTERFAZ EXTERNA -j ACCEPT
firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i INTERFAZ EXTERNA -o INTERFAZ INTERNA -m state --state RELATED,ESTABLISHED -j ACCEPT

## Allow nodes to reach specific services on the master
firewall-cmd --zone=cluster_internal --permanent --add-service=dhcp
firewall-cmd --zone=cluster_internal --permanent --add-service=http
firewall-cmd --zone=cluster_internal --permanent --add-service=https
firewall-cmd --zone=cluster_internal --permanent --add-service=tftp
firewall-cmd --zone=cluster_internal --permanent --add-service=ssh
firewall-cmd --zone=cluster_internal --permanent --add-service=nfs
firewall-cmd --zone=cluster_internal --permanent --add-service=mountd
firewall-cmd --zone=cluster_internal --permanent --add-service=rpc-bind

firewall-cmd --reload

firewall-cmd --list-all
firewall-cmd --list-all --zone=cluster_internal
```
Y luego se actualiza la red interna con la configuración nueva del firewall:

```bash
nmcli con mod INTERFAZ INTERNA connection.zone cluster_internal
nmcli con down INTERFAZ INTERNA
nmcli con up INTERFAZ INTERNA
```

## Ports de OpenPBS

Los sistemas de queuing basados en PBS (PBSPro, Torque, OpenPBS, etc.) usan TCP y UDP
en los ports 15001-15004. El cliente de pbs_mom también usa los puertos UDP 1023 y 
menores.
Problemas con el firewall podrían solucionarse explícitamente permitiendo esto:

```bash
firewall-cmd --zone=cluster_internal --permanent --add-port=15001-15004/udp
firewall-cmd --zone=cluster_internal --permanent --add-port=15001-15004/tcp
firewall-cmd --zone=cluster_internal --permanent --add-port=0-1023/udp
```
Esto último no está probado, use at your own risk.
