# Warewulf

Warewulf es un sistema de provisioning relativamente fácil de usar y con
un funcionamiento bastante estable. La inicialización se describe en la 
guía de instalación de OpenHPC, incluyendo la instalación de nodos
stateful (por defecto se utiliza una instalación headless).

> Nota: la versión de Warewulf que se usa en este caso es la 3, aunque la 4 ya está disponible y parece ser a donde marchan las cosas. OpenHPC todavía usa v3 pero es posible que esto cambie en el futuro. La sintaxis de la versión 4 es significativamente distinta y el backend está reescrito en Go, así que es posible que sea bastante diferente. Sin embargo, creo que aunque sea algunos parámetros básicos serán parecidos y el diagnóstico similar.

[Esta](https://www.admin-magazine.com/HPC/Articles/Warewulf-Cluster-Manager-Master-and-Compute-Nodes)
es una serie de artículos útiles sobre Warewulf usado únicamente como mecanismo de generación
de clusters.

## Regeneración del VNFS

Después de cada modificación al sistema de archivos del entorno `CHROOT`, es necesario 
regenerar el VNFS, operación que desgraciadamente puede ser muy lenta(unos minutos) 
para comprimir la imagen y agregarla a la base de datos. 
Es conveniente siempre tener definido en la variable de entorno `CHROOT` el directorio
sobre el que se trabaja (es posible tener múltiples para distintas imágenes, pero en este
caso nos referimos a la imagen básica del nodo de cómputo). Para eso se puede agregar al
`.bashrc` de root:

```bash
export CHROOT=/opt/ohpc/admin/images/rocky8.5
```
Y después se regenera el VNFS con

```bash
$ wwvnfs --chroot $CHROOT
```

## Instalación de paquetes/uso del chroot

Para instalar un paquete en el [chroot](https://wiki.archlinux.org/title/Chroot), puede 
ejecutarse simplemente 

```bash
$ yum install --installroot=$CHROOT somepackage
```
o, si se prefiere, hacerlo desde el mismo entorno de `CHROOT` instalando el package manager:

```bash
$ yum install --installroot=$CHROOT yum rpm rpmbuild dnf
```
Esto se hace una única vez, y luego se puede operar desde el chroot:

```bash
$ chroot $CHROOT
$[/] yum install somepackage 
$[/] exit
```
Después de instalar se regenera el VNFS y se reinician los nodos.

## Sincronización de archivos

Hay dos opciones para mantener los archivos en el sistema de archivos de los nodos. 
La primera es, si se quiere usar exactamente lo mismo que en el frontend , sincronizar
los archivos deseados a los del frontend:

```bash
$ wwsh file import /etc/passwd # ejemplo
```
El archivo puede fijarse a otra ubicación con `wwsh file set myfile --path=/new/path/myfile`.

Para forzar la sincronización inmediatamente después de algún cambio se puede ejecutar

```bash
$ wwsh file resync
```
o
```bash
$ wwsh file resync passwd shadow group
```
(Para sincronizar únicamente `passwd`,`shadow` y `group`.)
La propagación de los cambios realizados puede tardar ~5 minutos. Se puede forzar la 
sincronización manualmente con 

```bash
$ /warewulf/bin/wwgetfiles
```
en cada nodo (o usando `pdsh` para ejecutar simultáneamente en varios nodos). 

Otra opción es modificar los archivos directamente en el chroot, lo que permite una configuración
separada para los nodos.
Por ejemplo,
```bash
$ vim $CHROOT/etc/fstab
```
lo que puede ser útil para [montar directorios del frontend](Spack). También pueden modificarse los 
archivos desde el chroot:

```bash
$ chroot $CHROOT
$[/] vim /etc/auto.master
```
En el caso de modificar los archivos en el chroot, es necesario regenerar el VNFS y rebootear nodos.
El otro mecanismo es dinámico, por lo que pueden mantenerse config files por fuera del chroot en 
algún directorio específico y sincronizarlos dinámicamente para evitar regenerar el VNFS por 
modificaciones tan pequeñas.

## Diagnósticos
Durante el booteo en los nodos, es posible conectar un monitor al nodo en 
sí (tanto uno real como uno virtual si el nodo lo permite) y obtener mensajes
de error en el caso de que esto no se resuelva. Razones típicas para que el
sistema no bootee pueden ser que la configuración de red sea incorrecta, que el
kernel destinado al sistema no esté propiamente configurado, que PXE no esté 
configurado en la BIOS del nodo como primera opción, o que sencillamente alguna 
propiedad del nodo esté mal seteada.

Para diagnosticar algún problema es útil mirar el output de los siguientes comandos:

```bash
$ wwsh node print
#### compute-0-0 ##############################################################
    compute-0-0: ID               = 7
    compute-0-0: NAME             = compute-0-0
    compute-0-0: NODENAME         = compute-0-0
    compute-0-0: ARCH             = x86_64
    compute-0-0: CLUSTER          = UNDEF
    compute-0-0: DOMAIN           = UNDEF
    compute-0-0: GROUPS           = UNDEF
    compute-0-0: ENABLED          = TRUE
    compute-0-0: eno2.HWADDR      = b8:ac:6f:98:c5:61
    compute-0-0: eno2.HWPREFIX    = UNDEF
    compute-0-0: eno2.IPADDR      = 10.0.1.1
    compute-0-0: eno2.NETMASK     = UNDEF
    compute-0-0: eno2.NETWORK     = UNDEF
    compute-0-0: eno2.GATEWAY     = UNDEF
    compute-0-0: eno2.MTU         = UNDEF
    compute-0-0: eno2.FQDN        = UNDEF
    compute-0-0: eno2.BONDDEVS    = UNDEF
    compute-0-0: eno2.BONDMODE    = UNDEF
#### compute-0-1 ##############################################################
    compute-0-1: ID               = 9
    compute-0-1: NAME             = compute-0-1
    compute-0-1: NODENAME         = compute-0-1
    compute-0-1: ARCH             = x86_64
    compute-0-1: CLUSTER          = UNDEF
    compute-0-1: DOMAIN           = UNDEF
    compute-0-1: GROUPS           = UNDEF
    compute-0-1: ENABLED          = TRUE
    compute-0-1: eno2.HWADDR      = b8:ac:6f:98:c3:f6
    compute-0-1: eno2.HWPREFIX    = UNDEF
    compute-0-1: eno2.IPADDR      = 10.0.1.2
    compute-0-1: eno2.NETMASK     = UNDEF
    compute-0-1: eno2.NETWORK     = UNDEF
    compute-0-1: eno2.GATEWAY     = UNDEF
    compute-0-1: eno2.MTU         = UNDEF
    compute-0-1: eno2.FQDN        = UNDEF
    compute-0-1: eno2.BONDDEVS    = UNDEF
    compute-0-1: eno2.BONDMODE    = UNDEF
#### compute-0-2 ##############################################################
    compute-0-2: ID               = 10
    compute-0-2: NAME             = compute-0-2
    compute-0-2: NODENAME         = compute-0-2
    compute-0-2: ARCH             = x86_64
    compute-0-2: CLUSTER          = UNDEF
    compute-0-2: DOMAIN           = UNDEF
    compute-0-2: GROUPS           = UNDEF
    compute-0-2: ENABLED          = TRUE
    compute-0-2: eno2.HWADDR      = b8:ac:6f:98:ba:54
    compute-0-2: eno2.HWPREFIX    = UNDEF
    compute-0-2: eno2.IPADDR      = 10.0.1.3
    compute-0-2: eno2.NETMASK     = UNDEF
    compute-0-2: eno2.NETWORK     = UNDEF
    compute-0-2: eno2.GATEWAY     = UNDEF
    compute-0-2: eno2.MTU         = UNDEF
    compute-0-2: eno2.FQDN        = UNDEF
    compute-0-2: eno2.BONDDEVS    = UNDEF
    compute-0-2: eno2.BONDMODE    = UNDEF
(...)
```
Este comando especifica algunas propiedades básicas de los nodos. Como se puede ver, hay
algunas que si no es un cluster muy grande se pueden dejar sin definir y el funcionamiento
está bien igual. Críticamente es necesario un nombre, un ID, una arquitectura del procesador,
la MAC del nodo y una dirección IP (¡Distinta a la dirección IP de [IPMI](../Fundamentos/IPMI.md)! Por esta 
dirección va a bootear por PXE y es asignada por el frontend a partir de la MAC). La netmask
de la IP puede dejarse sin definir siempre y cuando esté definida en la configuración de red,
aunque quizás sería buena práctica definirla también. Estas propiedades pueden cambiarse a 
mano con `wwsh node ... set ...`.

```bash
$ wwsh provision print
#### compute-0-0 ##############################################################
    compute-0-0: MASTER           = UNDEF
    compute-0-0: BOOTSTRAP        = 4.18.0-348.el8.0.2.x86_64
    compute-0-0: VNFS             = rocky8.5
    compute-0-0: VALIDATE         = FALSE
    compute-0-0: FILES            = dynamic_hosts,group,network,passwd,shadow
    compute-0-0: PRESHELL         = FALSE
    compute-0-0: POSTSHELL        = FALSE
    compute-0-0: POSTNETDOWN      = FALSE
    compute-0-0: POSTREBOOT       = FALSE
    compute-0-0: CONSOLE          = UNDEF
    compute-0-0: PXELOADER        = UNDEF
    compute-0-0: IPXEURL          = UNDEF
    compute-0-0: SELINUX          = DISABLED
    compute-0-0: KARGS            = "net.ifnames=0 biosdevname=0"
    compute-0-0: BOOTLOCAL        = FALSE
#### compute-0-1 ##############################################################
    compute-0-1: MASTER           = UNDEF
    compute-0-1: BOOTSTRAP        = 4.18.0-348.el8.0.2.x86_64
    compute-0-1: VNFS             = rocky8.5
    compute-0-1: VALIDATE         = FALSE
    compute-0-1: FILES            = dynamic_hosts,group,network,passwd,shadow
    compute-0-1: PRESHELL         = FALSE
    compute-0-1: POSTSHELL        = FALSE
    compute-0-1: POSTNETDOWN      = FALSE
    compute-0-1: POSTREBOOT       = FALSE
    compute-0-1: CONSOLE          = UNDEF
    compute-0-1: PXELOADER        = UNDEF
    compute-0-1: IPXEURL          = UNDEF
    compute-0-1: SELINUX          = DISABLED
    compute-0-1: KARGS            = "net.ifnames=0 biosdevname=0"
    compute-0-1: BOOTLOCAL        = FALSE
```

Este comando contiene algunos parámetros clave para el provisioning. El bootstrap
correctamente formado debería haberse generado durante el proceso de instalación de 
OpenHPC y el root VNFS es un aspecto totalmente importante que debe estar bien definido
en todo momento. El parámetro `FILES` indica qué archivos están sincronizados con los 
archivos del frontend. Críticamente el parámetro `BOOTLOCAL` indica si se está booteando
desde el disco del nodo o desde el frontend mediante PXE.


El (o los múltiples) bootstrap además puede verificarse con

```bash
$ wwsh bootstrap print
BOOTSTRAP NAME            SIZE (M)      ARCH
4.18.0-348.el8.0.2.x86_64 45.8          x86_64
```
