# Sampling Drivers para Intel® VTune™


## Introducción

Intel® VTune™ utiliza kernel drivers para recolectar eventos por hardware usando la PMU (Performance Monitoring Unit) de las CPUs.

Debido a que en el labsim todavía usamos CPUs de más de 10 años de antigüedad, los drivers de VTune no son compatibles con ellos. Ya sea porque no cuentan con la PMU o porque no VTune no logra reconocer la información del CPU.

:::{seealso}
:class: dropdown

EBS analysis makes use of the on-chip Performance Monitoring Unit (PMU) and requires a genuine Intel® processor for collection. EBS analysis is supported on Intel® Xeon® Processors v3 (or later) family and 4th generation (or later) Intel® Core™ processors. Support for older processors is deprecated effective Intel® VTune™ Profiler 2022.0 release and will be removed in future. Note that a system does not need to support EBS analysis collection in order to open and view existing EBS results; systems that meet the less restrictive general operation requirements are sufficient for viewing results.
:::

## Identificar nodos con CPUs compatibles

Intel VTune provee un self-checker para testear las capacidades de profiling de nuestra arquitectura.

```bash
$ cd /opt/intel/oneapi/vtune/2025.0/bin64/
$ ./vtune-self-checker.sh
```
```bash
$ module load vtune
$ vtune-self-checker.sh
```

### Ejecución en todos los nodos con SLURM

Para testear la capacidad de todos los nodos, correr el siguiente script de Slurm.

```slurm
#!/bin/bash
#SBATCH --job-name="vtune check"
#SBATCH --output=vtune_check_%j_%N.out
#SBATCH -n 1

echo "self check being run on " $(hostname)

#Load modules
module purge
module load intel vtune

vtune-self-checker.sh
```

#### Resultados posibles
##### CPU compatible

En caso de que nuestro hardware permita hacer hardware sampling driver, dentro del output del self-checker debemos encontrar algo como:
```
 ...
 ...
HW event-based analysis (counting mode) (Intel driver)   
Example of analysis types: Performance Snapshot
Collection: Ok
Running finalization...
Finalization: Ok
Report: Ok
 ...
 ...
 ```

##### CPU Incompatible

En caso de error, dirá algo como esto:

```
...  
...  
HW event-based analysis (counting mode)   
Example of analysis types: Performance Snapshot
Collection: Fail
vtune: Error: This analysis type is not applicable to the system because 
VTune Profiler cannot recognize the processor.
...  
...  
```


:::{note}
Los nodos que no pueden realizar hardware event based sampling, pueden realizar software event based sampling.
:::

## Constraint de SLURM

Podemos crear una slurm feature para identificar los nodos que si pueden usar HW event-based analysis.

Editar `slurm.conf` para agregar el atributo `vtune-hw` a los nodos exitosos.

```
NodeName=compute-01 Features=vtune-hw CPUs=40 Sockets=2 CoresPerSocket=10 ThreadsPerCore=2
```

De esta forma, cuando un usuario quiera hacer hw-based sampling sobre su código, puede pedir un job en slurm que contenga la `constraint` respectiva.

{emphasize-lines="4"}
```slurm
#!/bin/bash
#SBATCH --job-name="profiling_example"
#SBATCH --output=profiling_example_output.out
#SBATCH --constraint=vtune-hw

#Load modules
module purge
module load intel vtune

vtune -collect hotspots -knob sampling-mode=hw -- /home/test/myApplication
```

## Build drivers

Para poder utilizar los sampling drivers, primero tenemos que buildearlos.

Para estos necesitamos el paquete `kernel-devel` correspondiente a nuestra versión del kernel identificable realizando el comando `uname -r`.

Descargar `kernel-devel` correspondiente a nuestra versión de kernel.

Descargar `.rpm` de un mirror confiable como por ejemplo:
`https://download.rockylinux.org/vault/`.

Una vez que tenemos descargado el .rpm correspondiente, instalarlo en el frontend:

```bash
$ rpm -i kernel-devel-5.14.0-427.16.1.el9_4.x86_64.rpm   
```

Instalar también en la imagen de los compute nodes.

```bash
$ rpm -i --root=$CHROOT kernel-devel-5.14.0-427.37.1.el9_4.x86_64.rpm
```

Editar `/etc/warewulf/vnfs.conf` y asegurarse que la linea `exclude += /usr/src` se encuentra comentada. `kernel-devel` se instala en este directorio y es necesario para poder cargar y usar los drivers.

Ir al siguiente directorio:

```bash
$ cd /opt/intel/oneapi/vtune/2025.0/sepdk/src/
```

Usar `build-driver` para construir el driver. Cuando nos pida el directorio del kernel, indicarle el directorio `/usr/src/kernels/<kernel version>`.

Usar `insmod-sep -q` para comprobar si los drivers se encuentran cargados en el sistema.


## Autoload Drivers

Cada vez que reiniciemos un nodo, los drivers no estarán cargados por default. 

Queremos asegurarnos que estén cargados, para eso necesitamos esperar que al bootear el nodo, se monte `/opt/intel` via NFS.

Systemd logra identificar este evento:

{emphasize-lines="7"}
```bash
[root@compute-01]# systemctl list-units --type=mount
  UNIT                                                              LOAD   ACTIVE SUB     DESCRIPTION                                        
  -.mount                                                           loaded active mounted Root Mount
  dev-hugepages.mount                                               loaded active mounted Huge Pages File System
  dev-mqueue.mount                                                  loaded active mounted POSIX Message Queue File System
  home.mount                                                        loaded active mounted /home
  opt-intel.mount                                                   loaded active mounted /opt/intel
  opt-ohpc-pub.mount                                                loaded active mounted /opt/ohpc/pub
  run-credentials-systemd\x2dsysctl.service.mount                   loaded active mounted /run/credentials/systemd-sysctl.service
  run-credentials-systemd\x2dsysusers.service.mount                 loaded active mounted /run/credentials/systemd-sysusers.service
  run-credentials-systemd\x2dtmpfiles\x2dsetup.service.mount        loaded active mounted /run/credentials/systemd-tmpfiles-setup.service
  run-credentials-systemd\x2dtmpfiles\x2dsetup\x2ddev.service.mount loaded active mounted /run/credentials/systemd-tmpfiles-setup-dev.service
  run-user-1000.mount                                               loaded active mounted /run/user/1000
  sys-fs-fuse-connections.mount                                     loaded active mounted FUSE Control File System
  sys-kernel-config.mount                                           loaded active mounted Kernel Configuration File System
  sys-kernel-debug.mount                                            loaded active mounted Kernel Debug File System
  sys-kernel-tracing.mount                                          loaded active mounted Kernel Trace File System
  var-lib-nfs-rpc_pipefs.mount                                      loaded active mounted RPC Pipe File System

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.
16 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.
```
Podemos crear un servicio que espere a que se monte `/opt/intel`.

```bash
[root@compute-01]# cat /etc/systemd/system/vtune-drivers.service 
```
```systemd
[Unit]
Description=Script to run after /opt/intel mount.
Requires=opt-intel.mount
After=opt-intel.mount
RequiresMountsFor=/opt/intel/
 
[Service]
ExecStart=/root/bin/load_vtune_sepdk_drivers.sh
 
[Install]
WantedBy=opt-intel.mount
```

Desde el frontend, tenemos que hacer `chroot` a `$CHROOT` y hacer `systemctl daemon-reload` y `systemctl enable vtune-drivers.service` o como le hayamos puesto de nombre.

El archivo `/root/bin/load?tune?sepdk_drivers.sh` simplemente es:

```bash
#!/bin/bash

# Make sure this command is run after /opt/intel is mounted on the client system.

/opt/intel/oneapi/vtune/2025.0/sepdk/src/insmod-sep -g vtune -pu
```

:::{caution}
Intel nos asegura que no hay overhead asociado a tener los drivers cargados todo el tiempo.

Esto no lo testeamos, en caso que no ser así, podríamos bindear la carga de los drivers a la carga del módulo vtune, de esta manera, solo se cargarían al hacer `module load vtune`.
:::

## Grupo VTUNE

Por la forma en la que buildeamos los drivers, solo los usuarios en el grupo `vtune` pueden usar esta herramienta.
Sino, habría que buildearla con todos los usuarios especificados.

Por esto, tenemos que crear el grupo vtune.

```
sudo groupadd vtune
```

Luego, cada vez que creemos un usuario debemos agregarlo al grupo `vtune`.

:::{note}
Hay que ver si no hay una mejor forma de hacer esto.
:::