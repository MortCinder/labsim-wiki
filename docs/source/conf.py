# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'labsim-wiki'
copyright = '2024, Laboratorio de Simulaciones - CNEA - Conicet'
author = 'Labsim Admin Team'
release = '0.1.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
                "myst_parser",
                "sphinx_togglebutton"]

myst_enable_extensions = [
            "colon_fence",
            "attrs_block",
        ]

templates_path = ['_templates']
exclude_patterns = []

language = 'es'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_book_theme'
html_static_path = ['_static']
html_logo = 'images/labsim-logo-resized.png'
html_title = 'LabSim Wiki'
