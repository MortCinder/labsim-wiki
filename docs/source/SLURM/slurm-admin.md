# SLURM Cheatsheet

## Cheatsheet

Cheatsheet para administrar slurm. 

### Levantar nodos
Si el nodo se encuentra en State=Drain.

```
scontrol: update NodeName=<node> State=DOWN Reason="undraining"
scontrol: update NodeName=<node> State=RESUME
```

Reutilizar comando para cambiar el estado de los nodos.


### Accounting

Para poder ver histórico de accounting con user.

```
sudo sacct -S 0101 -o JobID,JobName,User,State
```

### Lista de usuarios

```
sacctmgr show user format=User,DefaultAccount%20,AdminLevel%15
```

### Agregar usuario nuevo

```
sacctmgr add user <user> Account=<Grupo de trabajo>
```

### Información nodo

```
scontrol show node <nodename>
```
### Actualizar slurm.conf

Luego de actualizar slurm.conf
```
sudo wwsh file sync slurm.conf
sudo scontrol reconfigure
systemctl restart slurmctld
pdsh -w compute-[1-n] systemctl restart slurmd
```

### Resetear nodos via slurm
```
sudo scontrol reboot asap nextstate=resume reason=<reason> <nodelist>
```
### Modificar FairShare

Utilizamos potencias de 2 para determinar el fairshare de cada usuario.

```
sacctmgr modify user username set fairshare=1024
sacctmgr modify account accountname set fairshare=128
```

### Suspender jobs en ejecución

Se puede suspender un job en ejecución para liberar recursos en un nodo sin necesidad de matar el job. 

Si se desea inhabilitar el nodo, primero es necesario drenar el nodo. Luego, puede suspender los jobs del nodo para luego poder retomarlos sin perder el progreso obtenido.

```
sudo scontrol update NodeName=<node_name> State=DRAIN Reason="maintenance"
sudo scontrol suspend <list_of_job_ids>
```

Luego se pueden retomar los jobs o bien reencolarlos.

```
sudo scontrol resume <list_of_job_ids>
sudo scontrol requeue <list_of_job_ids>
```

### Cola rápida

Para implementar la cola rápida, modificamos las `PriorityJobFactor` y `PriorityTier` en la configuración de las particiones.

Para que estos valores sean representativos en el algoritmo de FairShare, debemos asignar valores fuertes a `PriorityWeightPartition`

```
PriorityWeightPartition=100
...
...
PartitionName=debug Nodes=ALL Default=YES MaxTime=15-00 State=UP PriorityJobFactor=1 PriorityTier=1
PartitionName=fast Nodes=compute-05 Default=NO MaxTime=06:00 State=UP PriorityJobFactor=10 PriorityTier=4
```

