���$      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�
Infiniband�h]�h	�Text����
Infiniband�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhKh�B/home/bruno/WIKI/labsim-wiki/docs/source/Fundamentos/Infiniband.md�hhhhubh	�	paragraph���)��}�(hX�  [Infiniband](https://www.wikiwand.com/en/InfiniBand) es una tecnología desarrollada por Mellanox (ahora parte de
NVIDIA). Puede alcanzar anchos de banda considerablemente mayores a Ethernet y, crucialmente, latencias mucho más bajas.
Hay distintos tipos de velocidades y protocolos soportados. Algo importante es que, de manera similar a los cables CAT
de Ethernet, hay distintas generaciones de Infiniband típicamente identificadas como ConnectX [1-7]. Actualmente, 
las interfaces correspondientes a ConnectX menor o igual a 3 están quedando obsoletas en End of Life (EOL) y perdiendo 
soporte. Las redes de Infiniband presentes en el laboratorio son ConnectX (sol117) y ConnectX 3(sol67).�h]�(h	�	reference���)��}�(h�
Infiniband�h]�h�
Infiniband�����}�(hh5hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��refuri��&https://www.wikiwand.com/en/InfiniBand�uh+h3hKhh,hh/hhubh�= es una tecnología desarrollada por Mellanox (ahora parte de�����}�(hh/hhhNhNubh�
�����}�(hh/hhhNhNubh�yNVIDIA). Puede alcanzar anchos de banda considerablemente mayores a Ethernet y, crucialmente, latencias mucho más bajas.�����}�(hh/hhhNhNubh�
�����}�(hh/hhhh,hK ubh�vHay distintos tipos de velocidades y protocolos soportados. Algo importante es que, de manera similar a los cables CAT�����}�(hh/hhhNhNubh�
�����}�(hh/hhhh,hK ubh�rde Ethernet, hay distintas generaciones de Infiniband típicamente identificadas como ConnectX [1-7]. Actualmente,�����}�(hh/hhhNhNubh�
�����}�(hh/hhhh,hK ubh�wlas interfaces correspondientes a ConnectX menor o igual a 3 están quedando obsoletas en End of Life (EOL) y perdiendo�����}�(hh/hhhNhNubh�
�����}�(hh/hhhh,hK ubh�gsoporte. Las redes de Infiniband presentes en el laboratorio son ConnectX (sol117) y ConnectX 3(sol67).�����}�(hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hKhh,hhhhubh.)��}�(h��Nota: Con la adquisición de Mellanox por parte de NVIDIA todavía no se migraron los foros de Mellanox 
(community.mellanox.com) y sólo están disponibles mediante la caché de Google.�h]�(h�gNota: Con la adquisición de Mellanox por parte de NVIDIA todavía no se migraron los foros de Mellanox�����}�(hhwhhhNhNubh�
�����}�(hhwhhhNhNubh�Q(community.mellanox.com) y sólo están disponibles mediante la caché de Google.�����}�(hhwhhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hK
hh,hhhhubh.)��}�(hX�  Hay dos instalaciones de Infiniband: OFED (y paquetes asociados, soporte genérico) y MLNX OFED (soporte de Mellanox).
Hasta ahora no pude hacer funcionar el soporte genérico por lo viejo de las placas, y sí MLNX OFED; que ya se usaba 
en el laboratorio. La instalación de MLNX OFED tiene múltiples opciones y posibles problemas, aunque está parcialmente 
documentado en la migración de sol117.�h]�(h�vHay dos instalaciones de Infiniband: OFED (y paquetes asociados, soporte genérico) y MLNX OFED (soporte de Mellanox).�����}�(hh�hhhNhNubh�
�����}�(hh�hhhNhNubh�uHasta ahora no pude hacer funcionar el soporte genérico por lo viejo de las placas, y sí MLNX OFED; que ya se usaba�����}�(hh�hhhNhNubh�
�����}�(hh�hhhh,hK ubh�yen el laboratorio. La instalación de MLNX OFED tiene múltiples opciones y posibles problemas, aunque está parcialmente�����}�(hh�hhhNhNubh�
�����}�(hh�hhhh,hK ubh�'documentado en la migración de sol117.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hKhh,hhhhubh.)��}�(h�)Algunos comandos útiles para Infiniband:�h]�h�)Algunos comandos útiles para Infiniband:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hKhh,hhhhubh	�literal_block���)��}�(hX&  $ ibstat # Datos de la interfaz
$ ibnodes # Datos de los nodos conectados por Infiniband, incluido el switch
$ ibnetdiscover # Usado inicialmente para encontrar nodos en la red
$ ibnetdiag # Diagnóstico extensivo, dependiente en algunos casos de las últimas actualizaciones sobre los drivers
�h]�hX&  $ ibstat # Datos de la interfaz
$ ibnodes # Datos de los nodos conectados por Infiniband, incluido el switch
$ ibnetdiscover # Usado inicialmente para encontrar nodos en la red
$ ibnetdiag # Diagnóstico extensivo, dependiente en algunos casos de las últimas actualizaciones sobre los drivers
�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��language��bash��	xml:space��preserve�uh+h�hh,hKhhhhubh.)��}�(hX�  Es muy útil la [documentación oficial de Mellanox OFED](https://docs.nvidia.com/networking/m/view-rendered-page.action?abstractPageId=12013389)
(en la versión correspondiente), y en menor medida la 
[documentación de RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/part-infiniband_and_rdma_networking).
A su vez, dada la obsolescencia de versiones anteriores, son notables el [patch de Mellanox OFED en OpenHPC](https://github.com/viniciusferrao/mlnxofed-patch)
y los [drivers de ELRepo](https://www.mail-archive.com/elrepo@lists.elrepo.org/msg03895.html) para el caso de otras 
implementaciones.�h]�(h�Es muy útil la �����}�(hh�hhhNhNubh4)��}�(h�'documentación oficial de Mellanox OFED�h]�h�'documentación oficial de Mellanox OFED�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�hC�Vhttps://docs.nvidia.com/networking/m/view-rendered-page.action?abstractPageId=12013389�uh+h3hKhh,hh�hhubh�
�����}�(hh�hhhNhNubh�6(en la versión correspondiente), y en menor medida la�����}�(hh�hhhNhNubh�
�����}�(hh�hhhh,hK ubh4)��}�(h�documentación de RedHat�h]�h�documentación de RedHat�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�hC��https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/part-infiniband_and_rdma_networking�uh+h3hKhh,hh�hhubh�.�����}�(hh�hhhNhNubh�
�����}�(hh�hhhh,hK ubh�IA su vez, dada la obsolescencia de versiones anteriores, son notables el �����}�(hh�hhhNhNubh4)��}�(h�!patch de Mellanox OFED en OpenHPC�h]�h�!patch de Mellanox OFED en OpenHPC�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�hC�0https://github.com/viniciusferrao/mlnxofed-patch�uh+h3hKhh,hh�hhubh�
�����}�(hh�hhhh,hK ubh�y los �����}�(hh�hhhNhNubh4)��}�(h�drivers de ELRepo�h]�h�drivers de ELRepo�����}�(hj*  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�hC�Bhttps://www.mail-archive.com/elrepo@lists.elrepo.org/msg03895.html�uh+h3hKhh,hh�hhubh� para el caso de otras�����}�(hh�hhhNhNubh�
�����}�(hh�hhhh,hK ubh�implementaciones.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hKhh,hhhhubeh}�(h!]��
infiniband�ah#]�h%]��
infiniband�ah']�h)]�uh+h
hKhh,hhhhubah}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j{  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��es��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�wordcount-words�h	�substitution_definition���)��}�(h�234�h]�h�234�����}�hj�  sbah}�(h!]�h#]�h%]��wordcount-words�ah']�h)]�uh+j�  hh,ub�wordcount-minutes�j�  )��}�(h�1�h]�h�1�����}�hj�  sbah}�(h!]�h#]�h%]��wordcount-minutes�ah']�h)]�uh+j�  hh,ubu�substitution_names�}�(�wordcount-words�j�  �wordcount-minutes�j�  u�refnames�}��refids�}��nameids�}�jP  jM  s�	nametypes�}�jP  �sh!}�jM  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhh�
myst_slugs�}�ub.