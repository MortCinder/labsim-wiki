# Agregado de nodos nuevos a sol67 - Soporte de hardware

A continuación documento la instalación de un nuevo nodo a sol67 y las dificultades que resultan de tener el sistema operativo (Rocks 6-CentOS 6) tan desactualizado. Con un poco de suerte una actualización a OpenHPC elimine problemas de drivers y falta de soporte sobre el hardware nuevo[1](¡Esto puede traer el problema inverso con hardware demasiado viejo!).

## Intento inicial y diagnóstico

Para agregar el hardware nuevo de L. Ermann después de montarlo físicamente en el rack correspondiente a su grupo, probé la manera usual de hacerlo con Rocks, `insert-ethers`. Lo que obtuve a partir de esto fueron dos(!) direcciones MAC que Rocks trataba de kickstartear (enviar por PXE el archivo kickstart.cgi que permite bootear el instalador de, en este caso, Rocks) pero que sencillamente no kickstarteaban y quedaban colgados hasta forzar la interrupción de `insert-ethers`. 
Para diagnosticar este problema, conecté un monitor por VGA directamente al nodo y repetí el proceso nuevamente. Lo que observé que se abría era una instalación de Rocks pero no automatizada i.e. no manejada por el frontend. Avanzando en esa instalación, vi que pedía crucialmente drivers para la placa de red. Esto me dio una pista de cuál podía ser el problema: la imagen del kernel que Rocks enviaba al nodo probablemente era demasiado vieja como para incorporar los drivers de la placa de red nueva (Intel X722) del nodo. Como consecuencia el nodo podía bootear inicialmente por PXE, pero la transferencia del archivo de kickstart requeriría utilizar en su funcionalidad más completa la placa de red y por lo tanto el nodo nunca realizaría el proceso de kickstarting. Cada vez que volví a intentar el proceso recordé borrar los nodos creados (compute-20-2, compute-20-3) con `rocks remove appliance compute compute-20-2`.
El problema, entonces, estaba en poder incorporar los drivers de la placa de red a la imagen de Rocks.

## Agregado de drivers a Rocks

Rocks dispone de un [tutorial](http://www.rocksclusters.org/assets/usersguides/roll-documentation/base/6.2/customization-driver.html)[2](Notar que si se siguen los comandos del tutorial al pie de la letra tienen un error en los directorios, dicen `mkdir src` y luego se refieren a una carpeta src que estaría en `/export/src/kernel/src`. Mejor clonar el repo del kernel en un directorio con nombre diferente para evitar confusiones. Varios comandos deben corregirse para compensar este error de la documentación con el path correcto. El repo del que se debe clonar el kernel iso es el que está en el GitHub de Rocks) para agregar drivers nuevo a la imagen que se provee a los nodos. Estas instrucciones requieren encontrar un paquete de drivers con su Makefile correspondiente (hay algunos que en lugar de Makefile usan un script de Bash). Para la placa Intel X722 encontré que el paquete de drivers [i40e](https://www.intel.com/content/www/us/en/download/18026/intel-network-adapter-driver-for-pcie-40-gigabit-ethernet-network-connections-under-linux.html) la soportaba y se encontraba en el formato correcto de Makefile. A partir de esto, es necesario recompilar el ISO de Rocks "kernel" (que, confusamente, contiene el sistema base de Rocks y no únicamente su kernel, aunque el kernel esté incluido), que a su vez recompila el kernel y el instalador (anaconda) de CentOS. Intenté seguir este proceso pero me encontré con el siguiente error al compilar:
```
# make rpm

...
make -C ../../loader
make[3]: Entering directory `/export/src/kernel/BUILD/rocks-boot-6.2/enterprise/6/loader'
bzcat anaconda-13.21.229.tar.bz2 | tar -x
bzcat: Can't open input file anaconda-13.21.229.tar.bz2: No such file or directory.
tar: This does not look like a tar archive
tar: Exiting with failure status due to previous errors
make[3]: *** [anaconda-13.21.229] Error 2
make[3]: Leaving directory `/export/src/kernel/BUILD/rocks-boot-6.2/enterprise/6/loader'
make[2]: *** [../../loader/loader] Error 2
make[2]: Leaving directory `/export/src/kernel/BUILD/rocks-boot-6.2/enterprise/6/images/x86_64'
make[1]: *** [build] Error 2
make[1]: Leaving directory `/export/src/kernel/BUILD/rocks-boot-6.2'
error: Bad exit status from /var/tmp/rpm-tmp.xZ3AWa (%build)


RPM build errors:
    Bad exit status from /var/tmp/rpm-tmp.xZ3AWa (%build)
make: *** [rpm] Error 1
```
Este error indica que la compilación fallaba no con el kernel sino con el instalador de CentOS (anaconda). Al parecer, el código fuente de anaconda no estaba incluido en el repo de Rocks. Es necesario, entonces, buscar el código fuente de anaconda para esa versión específica (otra versión seguro traería problemas de compatibilidad). Erróneamente probé desempaquetar un RPM de binarios de anaconda que venían con el sistema y reempaquetarlos en un tar.bz2, pero como no sabía que era el código fuente y no los binarios lo que necesitaba el compilador, el proceso falló:

```
# make rpm

...
bzcat anaconda-13.21.229.tar.bz2 | tar -x
cd patch-files && find anaconda-13.21.229 -type f | \
                grep -v CVS | cpio -pduv ../
..//anaconda-13.21.229/loader/urlinstall.c
..//anaconda-13.21.229/loader/kickstart.c
..//anaconda-13.21.229/loader/Makefile.am
..//anaconda-13.21.229/loader/init.c
..//anaconda-13.21.229/loader/loader.c
..//anaconda-13.21.229/loader/urls.c
..//anaconda-13.21.229/loader/net.c
..//anaconda-13.21.229/loader/loader.h
..//anaconda-13.21.229/loader/net.h
550 blocks
(                                               \
                cd anaconda-13.21.229;  \
                ./configure;                            \
                make;                                   \
        )
/bin/sh: line 2: ./configure: No such file or directory


```

Finalmente, pude encontrar el RPM con el código fuente googleando a partir de lo que mencionaba un [usuario en la lista de correos de Rocks](https://marc.info/?l=npaci-rocks-discussion&m=141579992132723&w=4). El archivo se encontraba en un viejo [FTP de RedHat](http://ftp.redhat.com/pub/redhat/linux/enterprise/6Client/en/os/SRPMS/) con el nombre `anaconda-13.21.229-1.el6.src.rpm`. Al extraer este RPM se obtiene el tar bz requerido, y se mueve a `/export/src/kernel/src/rocks-boot/enterprise/6/loader/anaconda-13.21.229.tar.bz2`.
Finalmente, con estas modificaciones, pude compilar y finalizar el proceso.
Ahora el nodo kickstarteaba (como compute-20-2), pero seguía apareciendo otra MAC que no kickstarteaba (compute-20-3) y seguía deteniendo el proceso de `insert-ethers`.

## Desactivación de la interfaz de IPMI

Después de repetir varias veces el proceso para ver la persistencia del problema, noté que la MAC que aparecía en `insert-ethers` era la misma que aparecía en la BIOS del nodo para la interfaz dedicada de IPMI. Aparentemente, aunque la interfaz de red estaba separada para IPMI (con su propio puerto), la BIOS estaba configurada para mezclar el tráfico de ambas interfaces por el mismo cable. Esto generaba el problema del nodo extra, y cambiar la configuración para que separe el tráfico solucionó el problema.

## Problemas de la interfaz gráfica

Ahora entonces el nuevo nodo kickstarteaba y se cerraba correctamente `insert-ethers`. Sin embargo, aunque compute-20-2 respondía los pings, rehusaba la conexión por ssh y al conectarle un monitor mostraba una pantalla en negro. Probé entonces reiniciar el nodo y observé que el problema estaba justo después de que en los mensajes se señalaba el acceso al modo gráfico. Supuse entonces que el problema estaba con el soporte para la tarjeta gráfica que estuviera usando el nodo. Busqué entonces si era posible desactivar la interfaz gráfica de anaconda desde la imagen, pero finalmente encontré que Rocks provee una opción para bootear en modo texto:

```
rocks set host installaction compute-20-2 action="install headless"
rocks set host boot compute-20-2 action=install
rocks list host
```

Finalmente con esto se instaló correctamente.

# Agregado a Torque/PBSNodes

```
qmgr -c "create node compute-20-2"
qmgr -c "set node compute-20-2 properties = fnt" # fnt propiedad del nodo de Ermann
pbsnodes -r compute-20-2
```
