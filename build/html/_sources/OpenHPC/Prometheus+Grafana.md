# Prometheus + Grafana

Para monitorear las distintas características y métricas del cluster (y poder identificar problemas de performance/
funcionamiento) es necesario colectar la información y mostrarla. [Ganglia](http://ganglia.sourceforge.net/) hace las
dos cosas, específicamente para sistemas de HPC. Sin embargo, Ganglia ya no tiene mantenimiento y correrlo en un sistema
moderno [puede ser más difícil](https://github.com/berlin2123/ganglia-web-docker).

Esto es una buena oportunidad para actualizar el stack de monitoreo a algo que sí esté mantenido y sea estándar. Por
un lado, para colectar la información, [Prometheus](https://prometheus.io/) como "aggregator"/proveedor de métricas y 
[Node Exporter](https://github.com/prometheus/node_exporter) como colector local en los nodos. Para realizar los 
gráficos con esas métricas, [Grafana](https://grafana.com/) es la opción más usada.

## Instalación Prometheus/Node Exporter

Primero hay que descargar los ejecutables de la página de [Releases](https://github.com/prometheus/prometheus/releases)

```bash
$ wget https://github.com/prometheus/prometheus/releases/download/v2.33.5/prometheus-2.33.5.linux-amd64.tar.gz
$ tar xvf prometheus-2.33.5.linux-amd64.tar.gz
$ wget https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz
$ tar xvf node_exporter-1.3.1.linux-amd64.tar.gz
```

Los ejecutables pueden usarse en las dos carpetas resultantes con:

```bash
$ ./prometheus --config.file=./prometheus.yml --web.listen-address=:9091 &
$ ./node_exporter &
```

Sin embargo, es necesario instalarlos servicios de systemd para poder tenerlos activos desde el startup del sistema.

Para instalar Node Exporter:

```bash
$ useradd --no-create-home --shell /bin/false node_exporter
$ cp node_exporter-1.3.1.linux-amd64/node_exporter /usr/local/bin/
```
Se crea el archivo de servicio de systemd en `/etc/systemd/system/node_exporter.service`:

```
[Unit]
Description=Node Exporter
After=network.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
```
Se habilita el servicio:

```bash
$ systemctl daemon-reload
$ systemctl enable node_exporter
```
y se inicia:

```
$ systemctl start node_exporter
```

Para verificar que esté funcionando correctamente (además de `systemctl status node_exporter`0) se usa
`curl http://localhost:9100/metrics` y en el caso de que esté funcionando correctamente por pantalla
se va a imprimir una lista de métricas con sus respectivas descripciones. 

Para instalar Prometheus:

```bash
$ useradd --no-create-home --shell /bin/false prometheus
$ mkdir /etc/prometheus
$ mkdir /var/lib/prometheus
$ chown prometheus:prometheus /etc/prometheus
$ chown prometheus:prometheus /var/lib/prometheus
$ cp prometheus-2.33.5.linux-amd64/prometheus /usr/local/bin/
$ cp prometheus-2.33.5.linux-amd64/promtool /usr/local/bin/
$ chown prometheus:prometheus /usr/local/bin/prometheus
$ chown prometheus:prometheus /usr/local/bin/promtool
$ cp -r prometheus-2.33.5.linux-amd64/consoles /etc/prometheus/
$ cp -r prometheus-2.33.5.linux-amd64/console_libraries/ /etc/prometheus/
$ chown -R prometheus:prometheus /etc/prometheus/
```

La configuración de Prometheus va en `/etc/prometheus/prometheus.yml`, que contiene:

```yaml
global:
  scrape_interval: 15s

scrape_configs:
- job_name: "prometheus"
  static_configs:
          - targets: ['localhost:9100']
            labels:
                    group: 'frontend'
          - targets: ['compute1:9100', 'compute2:9100', .....................]
            labels:
                    group: 'compute'
```

y se vuelven a verificar los permisos con `chown -R prometheus:prometheus /etc/prometheus/`.

En el campo `targets` van los nombres de cada compute y el puerto sobre el que va a correr `node_exporter`.
Para configurar el servicio de systemd, se crea el archivo correspondiente en `/etc/systemd/system/prometheus.service`:

```
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries \
    --web.listen-address=:9091
[Install]
WantedBy=multi-user.target
```
y se habilita para el próximo reboot con 

```bash
$ systemctl daemon-reload
$ systemctl enable prometheus
```
Se inicia para esta sesión:

```bash
$ systemctl start prometheus
```

## Instalar Node Exporter en los nodos

Node Exporter puede ejecutarse normalmente en los nodos. Pero también es conveniente armar un servicio para que inicie después 
de cada reboot.

La opción más fácil de hacerlo es modificando el VNFS copiando los archivos al chroot.

```bash
$ cp /usr/local/bin/node_exporter $CHROOT/usr/local/bin/
$ cp /etc/systemd/system/node_exporter.service $CHROOT/etc/systemd/system/
$ chown -R node_exporter:node_exporter $chroot/usr/local/bin/node_exporter
$ chroot $CHROOT
/ systemctl enable node_exporter
/ exit
$ wwsh file resync passwd shadow group
$ wwvnfs --chroot $CHROOT
```

donde `CHROOT` es la variable de entorno que tiene la ubicación del chroot de Warewulf.

Luego de la resincronización `wwsh file resync` debería tomar 5 minutos propagar los cambios.

En el próximo reboot de los nodos deberían iniciar `node_exporter` automáticamente. Para verificar esto,
puede accederse a Prometheus en el frontend entrando a la página `http://nombredelcluster.tandar.cnea.gov.ar:9091/targets?search=`
y ver qué instancias están activas.

## Instalación Grafana

Creamos el repo de Grafana (en el frontend)

```bash
$ vim /etc/yum.repos.d/grafana.repo
```

Insertamos el texto

```
[grafana]
name=grafana
baseurl=https://packages.grafana.com/oss/rpm
repo_gpgcheck=1
enabled=1
gpgcheck=1
gpgkey=https://packages.grafana.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
```

e instalamos con persistencia después de reboots:

```bash
$ yum install grafana
$ systemctl daemon-reload
$ systemctl start grafana-server
$ systemctl status grafana-server
$ systemctl enable grafana-server
```
Ingresamos a grafana en `http://nombredelcluster.tandar.cnea.gov.ar:3000` logueados como `admin:admin`. Configuramos Prometheus como fuente en
Connectinons > Data Sources > Prometheus: `http://localhost:9091` (el puerto es el elegido en la instalación de Prometheus, ver el servicio
configurado anteriormente).

A partir de esta fuente podemos crear dashboards. A partir de la versión 9 de Grafana se puede usar un Builder para las métricas que evita tener
que aprenderse demasiado de la sintaxis. No voy a cubrir demasiado de las métricas acá porque queda lo que hice para sol76 y sol117.

Después de seleccionar los dashboards/importar alguno es necesario ampliar la visibilidad para que los usuarios puedan acceder a los dashboards 
sin autenticarse. Para esto, se agregan las siguientes líneas a `/etc/grafana/grafana.ini`:

```
## Public view
[auth.anonymous]
enabled = true

# Organization name that should be used for unauthenticated users
org_name = Main Org.

# Role for unauthenticated users, other valid values are `Editor` and `Admin`
org_role = Viewer

# Hide the Grafana version text from the footer and help tooltip for unauthenticated users (default: false)
hide_version = true
```
y se reinicia el servicio del servidor de Grafana:

```bash
$ systemctl restart grafana-server
```

Para redirigir el tráfico del puerto 80 al puerto 3000, se puede cambiar la configuración de iptables:

```bash
$ iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 3000
```

AUNQUE ESTO NO ES RECOMENDABLE, PORQUE PUEDE TRAER PROBLEMAS CON WAREWULF.

Una alternativa es agregar una página que redirija al puerto 3000 en `/var/www/html/index.html`:

```html
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Redirecting to Grafana</title>
  </head>
  <body>
     <script>
		location.replace("http://sol117.tandar.cnea.gov.ar:3000")
     </script>
  </body>
</html>
```

o usando una tag meta de HTML, etc.

Para configurar las alarmas, usamos los webhooks de Discord. Se pueden configurar en el servidor
específico de Discord que queremos usar para las notificaciones (especificando canal, etc.) y 
después se configura como contact point de Grafana.
