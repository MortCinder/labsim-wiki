# Agregado de Drivers

Para agregar drivers en distribuciones derivadas de CentOS/RHEL, [ELRepo](http://elrepo.org/)
es el lugar a dónde ir, dado que su comunidad mantiene soporte para muchísimo hardware
que pierde el soporte oficial de RedHat. 

## Determinación de los drivers

Una vez que se determinó qué hardware no tiene soporte y requiere un driver nuevo, es necesario
usar un live environment para determinar el ID del device en particular:

```bash
$ lspci -nn 
(...)
03:00.0 SCSI storage controller [0100]: Broadcom / LSI SAS1068E PCI-Express Fusion-MPT SAS [1000:0058] (rev 08)
```
y se busca el número correspondiente (en este caso 1000:0058). A partir de este número, se puede buscar en 
[este sitio](http://elrepo.org/tiki/DeviceIDs) para encontrar el paquete correspondiente.

## Instalación de drivers

Para instalar drivers que necesitan soporte durante la instalación (por ejemplo, drivers
de SAS que son necesarios para reconocer los discos), es necesario agregar durante el proceso 
de booteo la opción que permite agregar los drivers al kernel. En la pantalla que se observa en
la imagen, se presiona TAB y se agrega

![boot menu](/images/boot-menu.png "Menú de booteo")

```
inst.dd=https://elrepo.org/linux/dud/el8/x86_64/dd-(...).elrepo.iso
```

al final de la opción de booteo, donde se buscó el paquete en formato ISO en 
[https://elrepo.org/linux/dud/el8/x86\_64/](https://elrepo.org/linux/dud/el8/x86_64/).

En el caso de que la instalación se pueda llevar a cabo normalmente, el agregado de paquetes de ELRepo
puede hacerse a través de `yum`:

```bash
$ rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
$ yum install https://www.elrepo.org/elrepo-release-8.el8.elrepo.noarch.rpm # RHEL8/CentOS8, revisar!
```

Con esto, los paquetes pueden instalarse simplemente con `yum install`.
