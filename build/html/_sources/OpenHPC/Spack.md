# Spack

Spack tiene una completa [documentación propia](https://spack.readthedocs.io/en/latest/),
que puede ser un tanto extensa. Es conveniente dejar constancia de algunas operaciones 
necesarias para poder usarlo correctamente.

## Uso básico
Spack se ocupa de mantener recetas de compilación para software científico con distintas
opciones y compiladores, mantener los paquetes en distintas versiones y generar los 
[modulefiles](/Fundamentos/Administración-de-software) correspondientes para poder mantener
en simultáneo versiones separadas.

Para buscar si un paquete `package` tiene receta de Spack, se usa

```bash
$ spack list package
```

(spack lista todos los paquetes disponibles y hace un match con los que se corresponden a esa
expresión)

Para mostrar la información y opciones de compilación:
```bash
$ spack info package
```
Para instalarlo:
```bash
$ spack install package@version^opt
```

A veces, la instalación no puede realizarse a través de fuentes remotas y es necesario descargar
(típicamente) un tar.gz en el directorio `/root`. Esto requiere también que el checksum del archivo
disponible sea el mismo que se usó para la receta de Spack. Si no es posible conseguir el mismo, 
puede usarse la opción `--no-checksum`.

## Habilitación de compiladores
Spack necesita ser configurado para agregar más compiladores disponibles a su suite. Si un compilador
fue instalado, es necesario correr

```bash
$ spack compiler find
$ spack compilers # check
```
(o a veces también `spack config edit compilers` para editar la configuración)
para que estén disponibles durante la compilación. Si los compiladores no son habilitados, pueden traer
errores como 

```
Error: OpenBLAS requires both C and Fortran compilers!
```
## Habilitación de modulefiles
Los modulefiles generados por spack pueden no aparecer por defecto en la instalación de LMod de OpenHPC, es
necesario decirle al sistema que agregue el `MODULEPATH` correspondiente.

### Frontend
En el frontend es necesario forzar el script de spack que fija las variables del entorno, entre ellas
el `MODULEPATH`. Para eso se agrega al `profile.d` de bash (que contiene "application-specific startup files")
el siguiente archivo

> Nota: Este método es bastante ineficiente porque consume tiempo del startup de bash (aprox 500ms), ver el update.

`/etc/profile.d/spack.sh`

```
#/bin/bash
# Load spack environment 
source /opt/ohpc/admin/spack/0.17.0/share/spack/setup-env.sh
```
(¡Revisar bien la versión y que el directorio exista!)
y se corre 

```
$ chmod +x /etc/profile.d/spack.sh
```
Para la sesión actual de bash se puede hacer el source a mano:

```bash
$ source /opt/ohpc/admin/spack/0.17.0/share/spack/setup-env.sh
```

#### Update
Buscando una manera eficiente de hacer esto encontré más fácil sumar el `MODULEPATH` al profile de lmod.

```bash
$ rm /etc/profile.d/spack.sh
$ vim /etc/profile.d/lmod.sh
``` 

Agrego la siguiente línea al final de `/etc/profile.d/lmod.sh`

```
export MODULEPATH=$MODULEPATH:/opt/ohpc/admin/spack/0.17.0/share/spack/modules/linux-rocky8-nehalem
```
y cierro y abro bash.


### Nodos
Con los nodos es necesario distribuir el filesystem que contiene spack a los nodos. Para esto, agrego la siguiente
línea a `/etc/exports`:

```
/opt/ohpc/admin *(ro,no_subtree_check,fsid=14)
```
(el parámetro `fsid=14` depende de cómo esté populado el archivo `/etc/exports`. En la 
[documentación de OpenHPC](https://github.com/openhpc/ohpc/releases/download/v2.4.GA/Install_guide-Rocky8-Warewulf-OpenPBS-2.4-x86_64.pdf),
sección *3.8.3 Customize system configuration*, muestra cómo se llena desde el principio)

Los nodos deben montarlo, por lo que es necesario agregar la siguiente línea a `$CHROOT/etc/fstab`:

```
10.0.0.1:/opt/ohpc/admin /opt/ohpc/admin nfs nfsvers=3,nodev 0 0
```
donde `10.0.0.1` es la IP particular de este frontend. Se regenera el VNFS de Warewulf y se rebootean los nodos.
Desde el frontend se corre

```bash
$ exportfs -a
```
y ya pasa a estar disponible.

> Nota: El paso siguiente también consume demasiado

Aparentemente el script de spack no parece estar reconociendo bien el filesystem montado como una opción para agregar el 
`MODULEPATH`, por eso los pasos anteriores pueden no funciona (o quizás haya algún problema más significativo). 
Para que funcione, se agrega manualmente el `MODULEPATH` en el archivo del profile:


`$CHROOT/etc/profile.d/spack.sh`

```
#/bin/bash
# Load spack environment 
source /opt/ohpc/admin/spack/0.17.0/share/spack/setup-env.sh
export MODULEPATH=$MODULEPATH:/opt/ohpc/admin/spack/0.17.0/share/spack/modules/linux-rocky8-nehalem
```
#### Update
Nuevamente borré `spack.sh` y agregué al final de `/etc/profile.d/lmod.sh`

```
export MODULEPATH=$MODULEPATH:/opt/ohpc/admin/spack/0.17.0/share/spack/modules/linux-rocky8-nehalem
```
 
Se vuelve a generar el VNFS y se rebootean los nodos.

## Personalización de los modulefiles

Los nombres que vienen por defecto en Spack son bastante molestos de leer, hice cambios a la estructura
[acorde a la documentación de Spack](https://spack.readthedocs.io/en/v0.17.0/module_file_support.html).
Hice una estructura tanto para el formato `tcl` como `lmod` en el archivo

`/opt/ohpc/admin/spack/0.17.0/etc/spack/modules.yaml`

```yaml
modules:
  default:
    enable:
     - lmod
    lmod:
      projections:
        all: '{name}/{version}'
        lammps: '{name}/{version}-{hash:1}'
      hash_length: 0
      core_compilers:
        - 'gcc@8.5.0'
      core_specs:
        - 'lammps'
      hierarchy:
        - 'mpi'
        - 'lapack'
    tcl:
      projections:
        all: '{name}/{version}-{compiler.name}-{compiler.version}'
        lammps: '{name}/{version}-{hash:1}-{compiler.name}-{compiler.version}'
        ^mpi: '{name}/{version}-{^mpi.name}-{^mpi.version}-{compiler.name}-{compiler.version}'
      hash_length: 0
```
y después regeneramos los módulos

```bash
$ spack module tcl refresh --delete-tree -y
```
Puede ser necesario regenerar la cache con 

```bash
$ rm .lmod.d/.cache/spiderT.x86_64_Linux.lua
```

## Mejora de performance en LMod

Si los módulos de LMod empiezan a tardar demasiado, se pueden seguir los 
[siguientes pasos](https://lmod.readthedocs.io/en/latest/130_spider_cache.html#how-to-test-the-spider-cache-generation-and-usage).
