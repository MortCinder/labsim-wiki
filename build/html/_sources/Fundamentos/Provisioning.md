# Provisioning

_Provisioning_ es el mecanismo por el cual se instala el sistema operativo en los nodos. Si un sistema de HPC requiriera
instalar una distribución de (GNU/)Linux por cada nodo e instalar cada programa de manera independiente, sería imposible
de mantener y escalar. Es por eso que, en general, los sistemas de HPC usan este mecanismo para bootear la imagen del 
sistema operativo mediante la interfaz de red descripta en [Estructura del cluster](/Fundamentos/Estructura-del-cluster). Esto se
hace mediante [PXE](https://www.wikiwand.com/en/Preboot_Execution_Environment), y requiere que la placa de red/BIOS del
nodo correspondiente lo soporte y lo tenga configurado apropiadamente como opción de booteo.
  
  Básicamente el nodo manda una request hacia el servidor (el frontend) y recibe, si todo sale bien, una imagen para 
bootear del sistema operativo y las instrucciones para instalarlo/hacer el setup. Esto se puede hacer de dos maneras:
_stateful_ o _stateless_/_headless_: La primera instala el sistema operativo en el disco del nodo, lo particiona y 
permite bootear desde ahí si no hay modificaciones significativas sobre la imagen del SO. La segunda instala siempre la 
imagen sobre RAM y es siempre una imagen "fresca". Hay distintos pros y contras para cada una, Rocks usa un stateful 
provisioning y Warewulf v3 en OpenHPC puede usar ambas. El panorama de provisioning parece estar moviendose hacia 
stateless y la v4 de Warewulf por ahora no ofrece la opción (xCAT seguramente sí).
  
  La idea con esto no es instalar el Sistema Operativo e ir haciendo modificaciones sobre él, sino que el paradigma está
centrado en una "golden image": una imagen del sistema operativo que se modifica para agregarle el software que se quiere
distribuir a los nodos. Los mecanismos por los que se hace esto son distintos según el sistema de provisioning, pero algo
que parecen tener en común en las opciones estudiadas es que el paso de generar la imagen puede ser computacionalmente 
intensivo y puede llevarle al frontend un par de minutos.
