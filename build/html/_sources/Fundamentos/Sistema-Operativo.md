# Sistema Operativo

Hay infinidad de soluciones de Sistema Operativo que permiten establecer un cluster de alto rendimiento. Incluso es 
posible que algún demente haya implementado uno usando Windows Server. Sin embargo el laboratorio, al igual que la 
inmensa mayoría de los centros de cómputo de alto rendimiento, usa algún sabor de (GNU/)Linux. Muchos de los centros 
de cómputo en el TOP 500 usan versiones de 
[RedHat/RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux) especialmente modificadas
para HPC, o diseñan su propio stack encima de otros como [OpenCATTUS](http://versatushpc.com.br/opencattus/) en Brasil.
Otras veces simplemente se toman distribuciones "vanilla" de Linux, se instala el software necesario a mano y se 
implementan soluciones particulares según las necesidades del centro de cómputo en cuestión.

  Sin embargo, también existen soluciones más especializadas, generales y abiertas. Este es el caso de Rocks Clusters
y OpenHPC.

## Rocks
[Rocks](https://www.rocksclusters.org/) es esencialmente es una distribución de Linux basada en CentOS (la rama libre de 
RHEL), con su propio mecanismo de distribución y su propia imagen para ser instalado monolíticamente una vez. Además 
cuenta con herramientas propias para realizar la mayoría de las tareas propias de un sistema de HPC (provisioning, 
manejo de software, manejo de nodos, NAS, etc.) a través de múltiples scripts de Python "baked into the system".
 Su conveniencia al tener todo en un mismo lugar es a su vez su mayor desventaja, porque por un lado es poco flexible a 
agregar herramientas externas y por otro es un single point of failure. El proyecto de Rocks, como tantos otros de 
Software Libre, quedó sin mantenimiento desde ~2017 y por lo tanto es imposible de reemplazar sin tomar el mando del 
proyecto y seguir con el desarrollo. La lista de correos de Rocks también deja mucho que desear y algunas soluciones son
bastante artesanales.

## OpenHPC
OpenHPC, por otro lado, no es una distribución de Linux per se. Es, esencialmente, un "meta-paquete" que contiene una 
navaja suiza de herramientas para armar un sistema HPC, junto a instrucciones para cómo hacerlo. Lo que Rocks gana en
centralización, OpenHPC lo gana en versatilidad. Al tratarse de un montón de herramientas individuales con desarrollo 
propio, un eventual fin del desarrollo de OpenHPC no representa una imposibilidad para reproducir/generar el deployment
de sistemas nuevos, dado que las herramientas siguen existiendo.
  Esto, naturalmente, trae sus propios problemas: el nivel de complejidad al tratar con múltiples herramientas es mayor,
y la curva de aprendizaje es más pronunciada que en el caso de Rocks [^1]. Igualmente, al ser tareas equivalentes, en la
práctica esto se traduce a mayor complejidad para _algunas_ tareas simplificadas en Rocks (p.ej. el setup de un NAS) y a
menor complejidad en el caso de las herramientas mejor diseñadas que las de Rocks (El provisioning y la administración de
software puede ser más fácil y sustancialmente mejor). Agregando a esto que es un sistema mucho más resiliente al paso del
tiempo no me cabe la menor duda de que la mejor opción, por ahora, es hacer la migración.

## Problemas con CentOS
Algo que no se discute en el inciso anterior es qué pasa con el sistema operativo base para OpenHPC; al fin y al cabo 
tiene que instalarse sobre un sistema ya existente y, por ejemplo, Ubuntu no es una opción soportada. Típicamente esto
se haría sobre CentOS8, pero recientemente la política de IBM/RedHat hizo modificaciones para pasar a CentOS de 
[downstream a upstream](https://www.theregister.com/2021/01/26/killing_centos/) y terminar el desarrollo de la 
distribución. En consecuencia, distribuciones como [Rocky Linux](https://www.theregister.com/2020/12/10/rocky_linux) y 
[AlmaLinux](https://blog.cloudlinux.com/announcing-open-sourced-community-driven-rhel-fork-by-cloudlinux) surgieron para
llenar el vacío que dejó la finalización de CentOS. Este momento es un período de transición para los sistemas basados 
en RedHat y es incierto qué solución permanecerá en el futuro. Lo bueno de OHPC es que es agnóstico a esta transición y 
aunque ahora se pueda implementar con soporte oficial en Rocky, cualquier cambio futuro puede adaptarse mientras queden
alternativas basadas en RHEL. 


[^1]: Todo esto por supuesto suponiendo que Rocks funcione a la primera y no se requiera ningún software o Roll adicional.
Esto no es cierto en la mayoría de los casos y en muchas ocasiones, particularmente con los Rolls y la administración de
Software, la curva de aprendizaje de OpenHPC me resultó mucho más agradable.
