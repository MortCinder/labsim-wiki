# IMPI

## Configuración de una red IPMI

IPMI es una herramienta central, en los nodos de Dell principalmente, para permitir
el apagado y (crucialmente) el encendido remoto de nodos. Configurar una red de IPMI
sin embargo tiene varias particularidades. Esencialmente, se busca usar una IP para
el servicio de IPMI de cada nodo separada de la IP normal del dispositivo, pero que
use la misma interfaz LAN (el mismo cable de red conectado al switch, con el tráfico
compartido).

## Configuración frontend

Nuevamente, lo que se quiere hacer es, dentro de la red local (típicamente `10.1.0.0`
o `10.1.1.0`), tener dentro de la misma red las direcciones de los nodos (p.ej. 
`10.1.1.254`) y las direcciones de los mismos nodos en IPMI (p.ej. `10.1.255.254`,
usamos todos los campos iguales menos el tercero para identificarlos como el mismo).
Para que esto sea posible, el sistema operativo tiene que:
1. Identificar a las dos direcciones dentro de la misma red.
2. Poder acceder a la nueva IP asignada a IPMI (la otra es estándar y se crea cuando
se agregan los nodos).
3. Tener un hostname asignado a esa IP.

Para 1., es necesario que la configuración de la red local tenga como gateway a 
`10.1.0.0` (o `10.1.1.0`, etc.) y **como netmask** `255.255.0.0` (en jerga, `/16`): 
esta netmask funciona como AND para los 4 números contra las direcciones de la red 
(AND contra 255 devuelve el valor del otro número y AND contra 0 devuelve 0). 
Por lo tanto, todas las direcciones de la forma `10.1.x.x` son "iguales" a `10.1.0.0` 
o `10.1.1.0`.

La netmask usualmente se fija durante la instalación del frontend. Sin embargo, también es 
posible cambiarla en el caso de que la provista durante la instalación fuera muy estricta
(por defecto es `255.255.255.0` -- en jerga `/24` --, i.e. sólo las direcciones de la forma 
`10.1.0.x` o `10.1.1.x` están en la misma red). Las herramientas para hacer esta operación 
dependen de lo que use la distribución. Típicamente 

```bash
ifconfig <nombre red> <gateway red> netmask 255.255.0.0
```
permite cambiarla temporalmente (y permanentemente editando 
`/etc/sysconfig/network-scripts/ifcfg-<nombre red>`), pero todo depende de la versión de 
Rocks/la distro utilizada. Rocks tiene además su propia 
[interfaz con las redes](http://central-7-0-x86-64.rocksclusters.org/roll-documentation/base/7.0/x1326.html)
que también puede ser necesario modificar (y `rocks sync config` siempre es necesario para 
aplicar los cambios a los nodos).

Luego de esto, la routing table de la red debería verse parecida a

```bash
$ netstat -rn
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
255.255.255.255 0.0.0.0         255.255.255.255 UH        0 0          0 eth0
192.168.200.194 10.1.1.1        255.255.255.255 UGH       0 0          0 eth0
224.0.0.0       0.0.0.0         255.255.255.0   U         0 0          0 eth0
192.168.200.0   0.0.0.0         255.255.252.0   U         0 0          0 eth1
10.1.0.0        0.0.0.0         255.255.0.0     U         0 0          0 eth0
0.0.0.0         192.168.200.254 0.0.0.0         UG        0 0          0 eth1
```
donde `eth0` es la red local y `eth1` es la red global (pueden ser también otras convenciones
como `em0`, etc.).

Para el punto 2., también es necesario realizar acciones en los nodos. Eso se describe en la 
sección siguiente.

Para el punto 3., es necesario agregar los hostnames de los "nuevos" dispositivos IPMI a 
`/etc/hosts`. Al finalizar este proceso, el archivo debería verse parecido a

```
10.1.255.252    compute-0-0.local       compute-0-0
10.1.255.253	compute-0-1.local	compute-0-1
(...)
10.1.120.252    compute-0-0-ipmi
10.1.120.253    compute-0-1-ipmi
(...)
```
Es decir, cada nodo tiene otro hostname (`<nodo>-ipmi`) con el tercer campo cambiado para
identificarlo como IPMI pero con el cuarto campo igual para identificarlo al mismo nodo. Los
primeros campos ya deberían estar en el archivo de hosts. Para agregar los últimos, se modifica 
(o se crea si no está) el archivo `/etc/hosts.local` y se agregan los hosts en el formato

```
10.1.<ipmi>.<fin-ip-nodo>	<nodo>-ipmi
```
donde `<ipmi>` es siempre el mismo número a elección entre 1 y 255 (120,1, etc.) y `<fin-ip-nodo>`
es el último campo de la ip del nodo. En este caso se eligió la terminación "-ipmi" para nombrarlos
pero se puede usar otra, siempre y cuando se ajusten los scripts correspondientes.

Para que `hosts.local` se agregue a `hosts`, basta con correr (en Rocks):

```bash
rocks sync config
```
y con eso ya se agregarán también a la configuración de los nodos.

## Configuración nodos
Para poder permitir la conexión por IPMI a cada nodo, es necesario habilitarlo desde la interfaz
de Dell para configurarlo: iDRAC (en el caso de los nodos Dell que hay en la sala de clusters) o iLO (en el caso de los HP ProLiant). 
Es posible ingresar a iDRAC mediante una interfaz web vinculada a la IP del dispositivo o más 
robustamente [en el menú durante el boot apretando F10, CTRL-E, etc.](https://www.dell.com/support/kbdoc/es-ar/000176910/c%C3%B3mo-iniciar-en-el-bios-o-en-el-lifecycle-controller-en-el-servidor-poweredge).

Las distintas versiones de iDRAC difieren en cómo los menúes están incorporados. Las opciones 
generales que deben modificarse a fin de permitir la conexión remota son:

* La IP del dispositivo IPMI se asigna manualmente al valor establecido en la sección anterior 
(`10.1.<ipmi>.<fin-ip-nodo>`), con la netmask de la red local si es necesario.
* Enable IPMI over LAN
* Enable iDRAC6 interface
* VLAN enable off (a veces rompe la funcionalidad si está activado)
* Configurar el login por defecto de IPMI (root:ADMIN) evita conflictos de autenticación que pasan
a veces con otras contraseñas ingresadas por hardware. Históricamente en el laboratorio se usa
root:calvin sin ningún problema. El problema puede darse más que nada cuando se usan caracteres 
especiales: la configuración del teclado en iDRAC probablemente sea alguna de las inglesas y la
conexión remota por IPMI usa la distribución local del teclado utilizado.

En el caso de iLO la configuración a veces es sólo accesible desde una interfaz web con IP que figura *durante el booteo* del nodo, a la que se puede acceder desde cualquier computadora en la red *siempre y cuando el puerto de iLO del nodo se encuentre conectado al switch correspondiente*. Para habilitar IPMI simplemente se activa la opción IPMI over LAN, se modifica su IP y se habilita un usuario para la autenticación correspondiente (es posible que no permita contraseñas inseguras como calvin). El primer ingreso a la interfaz web de iLO se realiza con las credenciales que figuran en un sticker pegado al nodo.

Después de esto (y de resetear algunas cosas) la conexión, en principio, debería funcionar 
correctamente. De no ser así es cuestión también de buscar y diagnosticar correctamente el 
problema que tiene la red (con `ipmitool -v` se suele poder ver si es un problema de conexión
o autenticación).
### Hardware
Para lo anterior se asume la configuración de los nodos Dell: Esto es, con iDRAC y una misma interfaz de red (una única [MAC Address](https://www.wikiwand.com/en/MAC_address)). Sin embargo, este no es el caso para todos los nodos. Algunos nodos directamente no tienen soporte para IPMI y no pueden ser configurados para soportarlo (personalmente todos los que yo probé se podían configurar con un poco de esfuerzo). Otros nodos, como es el caso de los HP ProLiant de sol67, usan una puerto dedicado (con la interfaz iLO) para broadcastear una IP que sí permite IPMI, por lo que requieren un cable adicional por nodo. Otros, como el Supermicro de sol67 (compute-20-2) permiten la configuración en BIOS del tráfico de IPMI por una misma interfaz o por una interfaz dedicada. 
