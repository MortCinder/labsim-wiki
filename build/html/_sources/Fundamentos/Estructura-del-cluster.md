# Estrucura del Cluster

Aunque estos conceptos están detallados en múltiples lugares, es útil tener presente la estructura de los 
clusters de alto rendimiento. Para eso la imagen siguiente (Tomada de la documentación de OpenHPC) me parece
suficientemente ilustrativa:

![Wiki](/images/cluster-arch.png)

La conformación más elemental de un cluster consiste, dentro de estos elementos, en:

* Un frontend, master node, sms o head node. Esto es, una computadora que se encarga de dirigir y administrar los
 recursos, software y cómputo del resto, dentro de la red interna que define al cluster. Este nodo es además la 
"puerta al exterior" del cluster y es con quien interactúa el usuario a través de la red externa, mediante ssh
u otro mecanismo.
* Nodos de cómputo. Su tarea es básicamente recibir "trabajos" y procesarlos, en el orden y de la manera en que lo 
indica el frontend. Típicamente ni siquiera usan demasiado almacenamiento interno sino que montan sistemas de archivos
como clientes de algún sistema de archivos distribuido (en el caso más simple, como clientes de un servidor NFS en el
frontend).
* Switch. Para conectar el frontend a los nodos, es necesario que haya una interfaz mutua que permita manejar los 
paquetes de red sucesivamente intercambiados entre el frontend y los nodos.

Esta configuración es suficiente para calificar como un cluster de cómputo. sol101 y sol105 esencialmente siguen este 
modelo sencillo.

Elementos que pueden sumarse dentro de este esquema son:
* RAID en el almacenamiento. Clusters como sol101 y sol105 dependen de la integridad de los discos duros para tener 
integridad en los datos. Para estos casos, en los que todos los datos están en un único frontend, es útil que los discos 
tengan algún tipo de redundancia en caso de falla física en alguno de ellos. Los niveles de RAID van de 0 a 10 
dependiendo del tradeoff entre integridad y redundancia. Algunas de las placas que implementan RAID usan SAS en lugar de
SATA para comunicarse con los discos.
* UPSs para la alimentación. Básicamente las Uninterruptible Power Supplies funcionan como batería de respaldo en caso de
cortes de luz y permiten el funcionamiento durante cortes cortos, o el apagado correcto en el caso de cortes largos.

Por fuera de este esquema se pueden agregar otros elementos, como también se observa en la figura:
* Redes de alto throughput o baja latencia, como Infiniband. Esto permite acelerar cómputos en los que la comunicación
entre nodos es demasiado frecuente como para usar Ethernet eficientemente. Requiere de placas de red específicas para 
Infiniband (u Omnipath, tecnología menos usual) y un Switch específicamente de Infiniband.
* Login node. Cuando el acceso al frontend se vuelve demasiado frecuente y demandado es útil poner un "segundo frontend"
que permita reducir la carga de la cantidad de usuarios.
* Server con Filesystem distribuido. Un Network Attached Storage Server o NAS es una de las opciones para esto. 
Esencialmente es uno o múltiples servidores con NFS y RAID exclusivamente dedicado al almacenamiento de los datos y la 
mantención de su integridad. El mantenimiento de los datos además puede realizarse con ZFS u otras alternativas. También
hay opciones más complejas como Lustre, BeeGFS, Ceph, etc. que están diseñadas para mejorar significativamente el 
throughput del acceso al disco y disminuir el bottleneck en I/O.
* IPMI o algún mecanismo de Management Interface. A veces puede realizarse a través del mismo cable que conecta por 
Ethernet a la red local o a veces sólo mediante una interfaz separada (a efectos prácticos, otro cable de red del nodo al switch). La interfaz permite encender o apagar nodos remotamente, medir la temperatura y otros sensores varios, etc.
Es necesario que el nodo en particular tenga soporte para IPMI: los nodos de Dell suelen tener una interfaz llamada iDRAC
y los de HP una llamada iLO.
