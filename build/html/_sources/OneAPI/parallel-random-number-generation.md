# Parallel Random Number Generation

Investigando sobre generación de números aleatorios en entornos altamente paralelizados, me encuentro con que esta [no es una tarea sencilla](https://codereview.stackexchange.com/a/181061). 

La buena noticia es que Intel OneAPI trae en su libreria MKL (Math Kernel Library) un paquete de funciones denominado Vector Statistics (VS) que incluye en la misma las funciones de generacion de números paralelos mencionadas en el Paper [Parallel Random Numbers: As Easy as 1, 2, 3](https://www.thesalmons.org/john/random123/papers/random123sc11.pdf). 

La documentación de Intel trae información de como implementar estas funciones tanto en Fortran como en C. 

En Fortran, la función call random_number() es _thread safe_, pero tengo la sospecha de que no tiene una buena performance. Vale la pena hacer un benchmark y comparar la performance de una simulación de Montecarlo con random_number de la librería de Fortran como con uno de los PRNG de Intel MKL.
