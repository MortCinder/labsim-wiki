# Optimización hibrida OpenMP y OpenMPI para Vasp

## Motivación ##

Actualmente el laboratorio de simulaciones utiliza VASP sin hacer uso de OpenMP. Paralelizando únicamente con MPI en 1 o más nodos. 

Nuestro objetivo es realizar un benchmark de distintas combinaciones de MPI Ranks y de OpenMP threads y concluir una combinación óptima.

## Requisitos ##

Los tests van a correrse en un nodo de cómputo de sol76 de 48 threads (Intel® Xeon® E5-2650 v4).
2 sockets. 12 cores por socket. 2 threads por core.

Debemos abstenernos a la cantidad física de cores (24).

Las combinaciones son:
- 24 MPI Ranks x 1 OpenMP Thread por rank
- 12 MPI Ranks x 2 OpenMP Threads por rank
- 8 MPI Ranks x 3 OpenMP Threads por rank
- 6 MPI Ranks x 4 OpenMP Threads por rank
- 4 MPI Ranks x 6 OpenMP Threads por rank
- 3 MPI Ranks x 8 OpenMP Threads por rank (problema: uno de los ranks abarca mas de un socket)
- 2 MPI Ranks x 12 OpenMP Threads por rank
- 1 MPI Ranks x 24 OpenMP Threads por rank (¿deberiamos tener un thread que abarque más de un socket?)

## Tests ##

Antes de ejecutar cada test, hay que habilidar el módulo correspondiente a vasp.
`module load vasp/6.3.2-openmpi`

Además hay que setear las variables de entorno para OpenMP:
- `export OMP_NUM_THREADS= ` cantidad de OpenMP Threads 
- `export OMP_PLACES=cores`
- `export OMP_PROC_BIND=close`
- `export OMP_STACKSIZE=512m

Pasamos los siguientes argumentos a mpirun:
- `-np : cantidad de ranks`
- `--map-by ppr:<ranks per socket>:socket:pe=<cores per rank>`
- `--bind-to core` (luego podemos probar otros binds)



## Conclusiones ##

Podemos ver que con 3 OpenMP threads por cada rank (8 ranks en total, 4 por cada socket)

![benchmark-tloop](vasp-mpiopenmp-tloop.png)
![benchmark-ttotal](vasp-mpiopenmp-ttot.png)

Si vemos el tiempo total, vemos que 2 OpenMP threads por cada rank(12 ranks en total, 6 por cada socket) tiene mejor tiempo real, aunque la diferencia es de menos de 1 segundo con respecto a la combinacion anterior.



Podemos concluir que combinar OpenMP con MPI representa una mejora de performance con respecto a paralelizar unicamente con MPI.

------

## Resultados de los Tests
### 24 MPI Ranks - 1 OpenMP Thread 
```
#!/bin/bash
module load vasp/6.3.2-openmpi

export OMP_NUM_THREADS=1
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_STACKSIZE=512m

time mpirun -v -np 24 --map-by ppr:12:socket:pe=1 --bind-to core --report-bindings vasp_std > test1.1.out 2>&1 &
```
```
real	43m15.396s
user	1032m44.963s
sys	1m2.625s
```
```
      LOOP:  cpu time      0.7686: real time      0.7769
      LOOP:  cpu time      0.7446: real time      0.7493
      LOOP:  cpu time      0.7834: real time      0.7879
      LOOP:  cpu time      0.8762: real time      0.8813
      LOOP:  cpu time     74.2439: real time     74.5871
      LOOP:  cpu time    147.2608: real time    147.8799
      LOOP:  cpu time    147.2306: real time    147.8595
      LOOP:  cpu time    147.3055: real time    147.9533
      LOOP:  cpu time    147.6660: real time    148.2931
      LOOP:  cpu time    148.0529: real time    148.6809
      LOOP:  cpu time    146.7053: real time    147.3432
      LOOP:  cpu time    147.1275: real time    147.7318
      LOOP:  cpu time    146.8569: real time    147.4649
      LOOP:  cpu time    146.7846: real time    147.3934
      LOOP:  cpu time    147.3058: real time    147.9391
      LOOP:  cpu time    147.3136: real time    147.9282
      LOOP:  cpu time    147.1313: real time    147.7430
      LOOP:  cpu time    146.8873: real time    147.5077
      LOOP:  cpu time    147.0971: real time    147.7532
      LOOP:  cpu time    146.8320: real time    147.4830
      LOOP:  cpu time    220.0674: real time    221.0367
```
```
 total amount of memory used by VASP MPI-rank0    50298. kBytes
=======================================================================

   base      :      30000. kBytes
   nonlr-proj:       4026. kBytes
   fftplans  :       3776. kBytes
   grid      :       2121. kBytes
   one-center:         31. kBytes
   HF        :        129. kBytes
   wavefun   :       5469. kBytes
   fock_wrk  :       4746. kBytes



 General timing and accounting informations for this job:
 ========================================================

                  Total CPU time used (sec):     2583.406
                            User time (sec):     2580.601
                          System time (sec):        2.805
                         Elapsed time (sec):     2594.852

                   Maximum memory used (kb):      428252.
                   Average memory used (kb):          N/A

                          Minor page faults:       303583
                          Major page faults:            0
                 Voluntary context switches:         4718
```
```
[compute-0-17:25356] MCW rank 0 bound to socket 0[core 0[hwt 0-1]]: [BB/../../../../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 1 bound to socket 0[core 1[hwt 0-1]]: [../BB/../../../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 2 bound to socket 0[core 2[hwt 0-1]]: [../../BB/../../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 3 bound to socket 0[core 3[hwt 0-1]]: [../../../BB/../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 4 bound to socket 0[core 4[hwt 0-1]]: [../../../../BB/../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 5 bound to socket 0[core 5[hwt 0-1]]: [../../../../../BB/../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 6 bound to socket 0[core 6[hwt 0-1]]: [../../../../../../BB/../../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 7 bound to socket 0[core 7[hwt 0-1]]: [../../../../../../../BB/../../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 8 bound to socket 0[core 8[hwt 0-1]]: [../../../../../../../../BB/../../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 9 bound to socket 0[core 9[hwt 0-1]]: [../../../../../../../../../BB/../..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 10 bound to socket 0[core 10[hwt 0-1]]: [../../../../../../../../../../BB/..][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 11 bound to socket 0[core 11[hwt 0-1]]: [../../../../../../../../../../../BB][../../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 12 bound to socket 1[core 12[hwt 0-1]]: [../../../../../../../../../../../..][BB/../../../../../../../../../../..]
[compute-0-17:25356] MCW rank 13 bound to socket 1[core 13[hwt 0-1]]: [../../../../../../../../../../../..][../BB/../../../../../../../../../..]
[compute-0-17:25356] MCW rank 14 bound to socket 1[core 14[hwt 0-1]]: [../../../../../../../../../../../..][../../BB/../../../../../../../../..]
[compute-0-17:25356] MCW rank 15 bound to socket 1[core 15[hwt 0-1]]: [../../../../../../../../../../../..][../../../BB/../../../../../../../..]
[compute-0-17:25356] MCW rank 16 bound to socket 1[core 16[hwt 0-1]]: [../../../../../../../../../../../..][../../../../BB/../../../../../../..]
[compute-0-17:25356] MCW rank 17 bound to socket 1[core 17[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../BB/../../../../../..]
[compute-0-17:25356] MCW rank 18 bound to socket 1[core 18[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../BB/../../../../..]
[compute-0-17:25356] MCW rank 19 bound to socket 1[core 19[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../BB/../../../..]
[compute-0-17:25356] MCW rank 20 bound to socket 1[core 20[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../BB/../../..]
[compute-0-17:25356] MCW rank 21 bound to socket 1[core 21[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../../BB/../..]
[compute-0-17:25356] MCW rank 22 bound to socket 1[core 22[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../../../BB/..]
[compute-0-17:25356] MCW rank 23 bound to socket 1[core 23[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../../../../BB]
```
```
 running   24 mpi-ranks, with    1 threads/rank
```


### 12 MPI Ranks - 2 OpenMP Thread 

```
module load vasp/6.3.2-openmpi

export OMP_NUM_THREADS=2
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_STACKSIZE=512m

time mpirun -v -np 12 --map-by ppr:6:socket:pe=2 --bind-to core --report-bindings vasp_std > test2.2.out 2>&1 &
```
```
real	32m10.617s
user	761m38.511s
sys	7m24.042s
```
```
      LOOP:  cpu time      3.0429: real time      1.5299
      LOOP:  cpu time      2.9222: real time      1.4685
      LOOP:  cpu time      3.3526: real time      1.6846
      LOOP:  cpu time      3.8270: real time      1.9227
      LOOP:  cpu time    117.7296: real time     59.0709
      LOOP:  cpu time    228.8378: real time    114.7931
      LOOP:  cpu time    229.0595: real time    114.9038
      LOOP:  cpu time    228.9931: real time    114.8741
      LOOP:  cpu time    229.0401: real time    114.8946
      LOOP:  cpu time    229.3301: real time    115.0383
      LOOP:  cpu time    228.7349: real time    114.7415
      LOOP:  cpu time    228.8409: real time    114.7942
      LOOP:  cpu time    228.5444: real time    114.6446
      LOOP:  cpu time    228.6677: real time    114.7080
      LOOP:  cpu time    228.7749: real time    114.7614
      LOOP:  cpu time    228.5193: real time    114.6358
      LOOP:  cpu time    228.5533: real time    114.6542
      LOOP:  cpu time    228.6634: real time    114.7109
      LOOP:  cpu time    228.4463: real time    114.5984
      LOOP:  cpu time    341.8360: real time    171.4809
```

```
 total amount of memory used by VASP MPI-rank0    67147. kBytes
=======================================================================

   base      :      30000. kBytes
   nonlr-proj:      10529. kBytes
   fftplans  :       6804. kBytes
   grid      :       5506. kBytes
   one-center:        155. kBytes
   HF        :        194. kBytes
   wavefun   :      11796. kBytes
   fock_wrk  :       2163. kBytes



 General timing and accounting informations for this job:
 ========================================================

                  Total CPU time used (sec):     3844.318
                            User time (sec):     3814.141
                          System time (sec):       30.177
                         Elapsed time (sec):     1930.214

                   Maximum memory used (kb):      495968.
                   Average memory used (kb):          N/A

                          Minor page faults:       431766
                          Major page faults:            0
                 Voluntary context switches:         3502
```

```
[compute-0-17:25848] MCW rank 0 bound to socket 0[core 0[hwt 0-1]], socket 0[core 1[hwt 0-1]]: [BB/BB/../../../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25848] MCW rank 1 bound to socket 0[core 2[hwt 0-1]], socket 0[core 3[hwt 0-1]]: [../../BB/BB/../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25848] MCW rank 2 bound to socket 0[core 4[hwt 0-1]], socket 0[core 5[hwt 0-1]]: [../../../../BB/BB/../../../../../..][../../../../../../../../../../../..]
[compute-0-17:25848] MCW rank 3 bound to socket 0[core 6[hwt 0-1]], socket 0[core 7[hwt 0-1]]: [../../../../../../BB/BB/../../../..][../../../../../../../../../../../..]
[compute-0-17:25848] MCW rank 4 bound to socket 0[core 8[hwt 0-1]], socket 0[core 9[hwt 0-1]]: [../../../../../../../../BB/BB/../..][../../../../../../../../../../../..]
[compute-0-17:25848] MCW rank 5 bound to socket 0[core 10[hwt 0-1]], socket 0[core 11[hwt 0-1]]: [../../../../../../../../../../BB/BB][../../../../../../../../../../../..]
[compute-0-17:25848] MCW rank 6 bound to socket 1[core 12[hwt 0-1]], socket 1[core 13[hwt 0-1]]: [../../../../../../../../../../../..][BB/BB/../../../../../../../../../..]
[compute-0-17:25848] MCW rank 7 bound to socket 1[core 14[hwt 0-1]], socket 1[core 15[hwt 0-1]]: [../../../../../../../../../../../..][../../BB/BB/../../../../../../../..]
[compute-0-17:25848] MCW rank 8 bound to socket 1[core 16[hwt 0-1]], socket 1[core 17[hwt 0-1]]: [../../../../../../../../../../../..][../../../../BB/BB/../../../../../..]
[compute-0-17:25848] MCW rank 9 bound to socket 1[core 18[hwt 0-1]], socket 1[core 19[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../BB/BB/../../../..]
[compute-0-17:25848] MCW rank 10 bound to socket 1[core 20[hwt 0-1]], socket 1[core 21[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../BB/BB/../..]
[compute-0-17:25848] MCW rank 11 bound to socket 1[core 22[hwt 0-1]], socket 1[core 23[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../../../BB/BB]
```
```
 running   12 mpi-ranks, with    2 threads/rank
```

### 8 MPI Ranks - 3 OpenMP Thread 

```
module load vasp/6.3.2-openmpi

export OMP_NUM_THREADS=3
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_STACKSIZE=512m

time mpirun -v -np 8 --map-by ppr:4:socket:pe=3 --bind-to core --report-bindings vasp_std > test4.1.out 2>&1 &
```
```
real	32m31.367s
user	767m37.196s
sys	9m47.189s
```
```
      LOOP:  cpu time      3.5967: real time      1.2053
      LOOP:  cpu time      3.4487: real time      1.1553
      LOOP:  cpu time      3.8368: real time      1.2851
      LOOP:  cpu time      4.4146: real time      1.4786
      LOOP:  cpu time    167.4857: real time     56.0004
      LOOP:  cpu time    328.7926: real time    109.9295
      LOOP:  cpu time    328.7088: real time    109.9008
      LOOP:  cpu time    328.5422: real time    109.8455
      LOOP:  cpu time    328.2127: real time    109.7330
      LOOP:  cpu time    327.8669: real time    109.6182
      LOOP:  cpu time    328.6396: real time    109.8761
      LOOP:  cpu time    327.9772: real time    109.6424
      LOOP:  cpu time    328.5724: real time    109.8368
      LOOP:  cpu time    328.4838: real time    109.8059
      LOOP:  cpu time    328.9057: real time    109.9538
      LOOP:  cpu time    328.1762: real time    109.7246
      LOOP:  cpu time    328.5116: real time    109.8376
      LOOP:  cpu time    328.5563: real time    109.8506
      LOOP:  cpu time    329.4180: real time    110.1387
      LOOP:  cpu time    328.5832: real time    109.8591
      LOOP:  cpu time    492.0439: real time    164.5101
```
```
 total amount of memory used by VASP MPI-rank0    77558. kBytes
=======================================================================

   base      :      30000. kBytes
   nonlr-proj:      10984. kBytes
   fftplans  :       9708. kBytes
   grid      :       6758. kBytes
   one-center:        155. kBytes
   HF        :        194. kBytes
   wavefun   :      16515. kBytes
   fock_wrk  :       3244. kBytes



 General timing and accounting informations for this job:
 ========================================================

                  Total CPU time used (sec):     5829.301
                            User time (sec):     5767.348
                          System time (sec):       61.954
                         Elapsed time (sec):     1950.994

                   Maximum memory used (kb):      524060.
                   Average memory used (kb):          N/A

                          Minor page faults:       526890
                          Major page faults:            0
                 Voluntary context switches:         3295
```




```
[compute-0-17:26161] MCW rank 0 bound to socket 0[core 0[hwt 0-1]], socket 0[core 1[hwt 0-1]], socket 0[core 2[hwt 0-1]]: [BB/BB/BB/../../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:26161] MCW rank 1 bound to socket 0[core 3[hwt 0-1]], socket 0[core 4[hwt 0-1]], socket 0[core 5[hwt 0-1]]: [../../../BB/BB/BB/../../../../../..][../../../../../../../../../../../..]
[compute-0-17:26161] MCW rank 2 bound to socket 0[core 6[hwt 0-1]], socket 0[core 7[hwt 0-1]], socket 0[core 8[hwt 0-1]]: [../../../../../../BB/BB/BB/../../..][../../../../../../../../../../../..]
[compute-0-17:26161] MCW rank 3 bound to socket 0[core 9[hwt 0-1]], socket 0[core 10[hwt 0-1]], socket 0[core 11[hwt 0-1]]: [../../../../../../../../../BB/BB/BB][../../../../../../../../../../../..]
[compute-0-17:26161] MCW rank 4 bound to socket 1[core 12[hwt 0-1]], socket 1[core 13[hwt 0-1]], socket 1[core 14[hwt 0-1]]: [../../../../../../../../../../../..][BB/BB/BB/../../../../../../../../..]
[compute-0-17:26161] MCW rank 5 bound to socket 1[core 15[hwt 0-1]], socket 1[core 16[hwt 0-1]], socket 1[core 17[hwt 0-1]]: [../../../../../../../../../../../..][../../../BB/BB/BB/../../../../../..]
[compute-0-17:26161] MCW rank 6 bound to socket 1[core 18[hwt 0-1]], socket 1[core 19[hwt 0-1]], socket 1[core 20[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../BB/BB/BB/../../..]
[compute-0-17:26161] MCW rank 7 bound to socket 1[core 21[hwt 0-1]], socket 1[core 22[hwt 0-1]], socket 1[core 23[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../../BB/BB/BB]
```
```
 running    8 mpi-ranks, with    3 threads/rank
```

### 6 MPI Ranks - 4 OpenMP Thread 

```
module load vasp/6.3.2-openmpi

export OMP_NUM_THREADS=4
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_STACKSIZE=512m

time mpirun -v -np 6 --map-by ppr:3:socket:pe=4 --bind-to core --report-bindings vasp_std > test5.out 2>&1 &
```
```
real	32m57.158s
user	776m37.617s
sys	10m58.225s
```
```
      LOOP:  cpu time      3.9039: real time      0.9801
      LOOP:  cpu time      3.8343: real time      0.9623
      LOOP:  cpu time      3.9433: real time      0.9896
      LOOP:  cpu time      4.9875: real time      1.2515
      LOOP:  cpu time    224.2606: real time     56.2222
      LOOP:  cpu time    445.7548: real time    111.7559
      LOOP:  cpu time    444.8421: real time    111.5246
      LOOP:  cpu time    444.6903: real time    111.4865
      LOOP:  cpu time    445.9297: real time    111.7971
      LOOP:  cpu time    444.5620: real time    111.4560
      LOOP:  cpu time    445.3761: real time    111.6618
      LOOP:  cpu time    445.4620: real time    111.6793
      LOOP:  cpu time    445.6784: real time    111.7368
      LOOP:  cpu time    444.8543: real time    111.5294
      LOOP:  cpu time    445.3590: real time    111.6548
      LOOP:  cpu time    445.6068: real time    111.7184
      LOOP:  cpu time    445.5146: real time    111.6943
      LOOP:  cpu time    444.8496: real time    111.5274
      LOOP:  cpu time    446.0580: real time    111.8293
      LOOP:  cpu time    445.2150: real time    111.6201
      LOOP:  cpu time    665.7071: real time    166.8975
```
```
 total amount of memory used by VASP MPI-rank0    87963. kBytes
=======================================================================

   base      :      30000. kBytes
   nonlr-proj:      10613. kBytes
   fftplans  :      13018. kBytes
   grid      :       8427. kBytes
   one-center:        155. kBytes
   HF        :        194. kBytes
   wavefun   :      21231. kBytes
   fock_wrk  :       4325. kBytes



 General timing and accounting informations for this job:
 ========================================================

                  Total CPU time used (sec):     7874.792
                            User time (sec):     7779.311
                          System time (sec):       95.482
                         Elapsed time (sec):     1976.803

                   Maximum memory used (kb):      608824.
                   Average memory used (kb):          N/A

                          Minor page faults:       529214
                          Major page faults:            0
                 Voluntary context switches:         3247

```

```
[compute-0-17:26484] MCW rank 0 bound to socket 0[core 0[hwt 0-1]], socket 0[core 1[hwt 0-1]], socket 0[core 2[hwt 0-1]], socket 0[core 3[hwt 0-1]]: [BB/BB/BB/BB/../../../../../../../..][../../../../../../../../../../../..]
[compute-0-17:26484] MCW rank 1 bound to socket 0[core 4[hwt 0-1]], socket 0[core 5[hwt 0-1]], socket 0[core 6[hwt 0-1]], socket 0[core 7[hwt 0-1]]: [../../../../BB/BB/BB/BB/../../../..][../../../../../../../../../../../..]
[compute-0-17:26484] MCW rank 2 bound to socket 0[core 8[hwt 0-1]], socket 0[core 9[hwt 0-1]], socket 0[core 10[hwt 0-1]], socket 0[core 11[hwt 0-1]]: [../../../../../../../../BB/BB/BB/BB][../../../../../../../../../../../..]
[compute-0-17:26484] MCW rank 3 bound to socket 1[core 12[hwt 0-1]], socket 1[core 13[hwt 0-1]], socket 1[core 14[hwt 0-1]], socket 1[core 15[hwt 0-1]]: [../../../../../../../../../../../..][BB/BB/BB/BB/../../../../../../../..]
[compute-0-17:26484] MCW rank 4 bound to socket 1[core 16[hwt 0-1]], socket 1[core 17[hwt 0-1]], socket 1[core 18[hwt 0-1]], socket 1[core 19[hwt 0-1]]: [../../../../../../../../../../../..][../../../../BB/BB/BB/BB/../../../..]
[compute-0-17:26484] MCW rank 5 bound to socket 1[core 20[hwt 0-1]], socket 1[core 21[hwt 0-1]], socket 1[core 22[hwt 0-1]], socket 1[core 23[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../../../BB/BB/BB/BB]
```
```
 running    6 mpi-ranks, with    4 threads/rank
```

### 4 MPI Ranks - 6 OpenMP Thread 

```
module load vasp/6.3.2-openmpi

export OMP_NUM_THREADS=6
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_STACKSIZE=512m

time mpirun -v -np 4 --map-by ppr:2:socket:pe=6 --bind-to core --report-bindings vasp_std > test6.out 2>&1 &
```
```
real	33m59.939s
user	801m34.376s
sys	10m49.331s
```
```
      LOOP:  cpu time      5.3000: real time      0.8876
      LOOP:  cpu time      4.9470: real time      0.8276
      LOOP:  cpu time      5.3255: real time      0.8906
      LOOP:  cpu time      5.9792: real time      0.9997
      LOOP:  cpu time    350.0003: real time     58.4886
      LOOP:  cpu time    692.9392: real time    115.7814
      LOOP:  cpu time    688.8469: real time    115.1153
      LOOP:  cpu time    691.6414: real time    115.5836
      LOOP:  cpu time    690.7992: real time    115.4437
      LOOP:  cpu time    690.0756: real time    115.3227
      LOOP:  cpu time    689.3958: real time    115.2049
      LOOP:  cpu time    687.8887: real time    114.9541
      LOOP:  cpu time    688.2339: real time    115.0121
      LOOP:  cpu time    689.4829: real time    115.2216
      LOOP:  cpu time    690.9445: real time    115.4644
      LOOP:  cpu time    691.6345: real time    115.5840
      LOOP:  cpu time    688.0341: real time    114.9775
      LOOP:  cpu time    687.3893: real time    114.8694
      LOOP:  cpu time    688.0194: real time    114.9755
      LOOP:  cpu time    688.3598: real time    115.0316
      LOOP:  cpu time   1030.5349: real time    172.2166
```
```
 total amount of memory used by VASP MPI-rank0   108718. kBytes
=======================================================================

   base      :      30000. kBytes
   nonlr-proj:      11128. kBytes
   fftplans  :      19153. kBytes
   grid      :      10931. kBytes
   one-center:        155. kBytes
   HF        :        194. kBytes
   wavefun   :      30669. kBytes
   fock_wrk  :       6488. kBytes



 General timing and accounting informations for this job:
 ========================================================

                  Total CPU time used (sec):    12185.535
                            User time (sec):    12036.943
                          System time (sec):      148.592
                         Elapsed time (sec):     2039.489

                   Maximum memory used (kb):      710692.
                   Average memory used (kb):          N/A

                          Minor page faults:       676284
                          Major page faults:            0
                 Voluntary context switches:         6585

```
```
[compute-0-17:26864] MCW rank 0 bound to socket 0[core 0[hwt 0-1]], socket 0[core 1[hwt 0-1]], socket 0[core 2[hwt 0-1]], socket 0[core 3[hwt 0-1]], socket 0[core 4[hwt 0-1]], socket 0[core 5[hwt 0-1]]: [BB/BB/BB/BB/BB/BB/../../../../../..][../../../../../../../../../../../..]
[compute-0-17:26864] MCW rank 1 bound to socket 0[core 6[hwt 0-1]], socket 0[core 7[hwt 0-1]], socket 0[core 8[hwt 0-1]], socket 0[core 9[hwt 0-1]], socket 0[core 10[hwt 0-1]], socket 0[core 11[hwt 0-1]]: [../../../../../../BB/BB/BB/BB/BB/BB][../../../../../../../../../../../..]
[compute-0-17:26864] MCW rank 2 bound to socket 1[core 12[hwt 0-1]], socket 1[core 13[hwt 0-1]], socket 1[core 14[hwt 0-1]], socket 1[core 15[hwt 0-1]], socket 1[core 16[hwt 0-1]], socket 1[core 17[hwt 0-1]]: [../../../../../../../../../../../..][BB/BB/BB/BB/BB/BB/../../../../../..]
[compute-0-17:26864] MCW rank 3 bound to socket 1[core 18[hwt 0-1]], socket 1[core 19[hwt 0-1]], socket 1[core 20[hwt 0-1]], socket 1[core 21[hwt 0-1]], socket 1[core 22[hwt 0-1]], socket 1[core 23[hwt 0-1]]: [../../../../../../../../../../../..][../../../../../../BB/BB/BB/BB/BB/BB]
```
```
 running    4 mpi-ranks, with    6 threads/rank
```

### 2 MPI Ranks - 12 OpenMP Thread 

```
module load vasp/6.3.2-openmpi

export OMP_NUM_THREADS=12
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_STACKSIZE=512m

time mpirun -v -np 2 --map-by ppr:1:socket:pe=12 --bind-to core --report-bindings vasp_std > test8.out 2>&1 &
```
      LOOP:  cpu time     12.8799: real time      1.0797
      LOOP:  cpu time     11.9722: real time      1.0001
      LOOP:  cpu time     12.8861: real time      1.0764
      LOOP:  cpu time     14.4882: real time      1.2104
      LOOP:  cpu time    972.2519: real time     81.2086
      LOOP:  cpu time   1905.9375: real time    159.2128
      LOOP:  cpu time   1904.4873: real time    159.0932
      LOOP:  cpu time   1900.9939: real time    158.8055
      LOOP:  cpu time   1896.5440: real time    158.4307
      LOOP:  cpu time   1914.5856: real time    159.9360
      LOOP:  cpu time   1903.4001: real time    159.0033
      LOOP:  cpu time   1902.3725: real time    158.9164
      LOOP:  cpu time   1910.4752: real time    159.5935
      LOOP:  cpu time   1908.5941: real time    159.4382
      LOOP:  cpu time   1908.4191: real time    159.4230
      LOOP:  cpu time   1901.6040: real time    158.8530
      LOOP:  cpu time   1909.6511: real time    159.5244
      LOOP:  cpu time   1908.3047: real time    159.4140
      LOOP:  cpu time   1901.4980: real time    158.8445
      LOOP:  cpu time   1899.0009: real time    158.6393
      LOOP:  cpu time   2850.8075: real time    238.1478
```
real	46m37.870s
user	1100m23.738s
sys	14m13.262s
```
```
 total amount of memory used by VASP MPI-rank0   174594. kBytes
=======================================================================

   base      :      30000. kBytes
   nonlr-proj:      12610. kBytes
   fftplans  :      38042. kBytes
   grid      :      19276. kBytes
   one-center:        155. kBytes
   HF        :        194. kBytes
   wavefun   :      61341. kBytes
   fock_wrk  :      12976. kBytes



 General timing and accounting informations for this job:
 ========================================================

                  Total CPU time used (sec):    33439.883
                            User time (sec):    33050.232
                          System time (sec):      389.652
                         Elapsed time (sec):     2797.509

                   Maximum memory used (kb):     1054208.
                   Average memory used (kb):          N/A

                          Minor page faults:       782110
                          Major page faults:            0
                 Voluntary context switches:         3150

```
```
[compute-0-17:27300] MCW rank 0 bound to socket 0[core 0[hwt 0-1]], socket 0[core 1[hwt 0-1]], socket 0[core 2[hwt 0-1]], socket 0[core 3[hwt 0-1]], socket 0[core 4[hwt 0-1]], socket 0[core 5[hwt 0-1]], socket 0[core 6[hwt 0-1]], socket 0[core 7[hwt 0-1]], socket 0[core 8[hwt 0-1]], socket 0[core 9[hwt 0-1]], socket 0[core 10[hwt 0-1]], socket 0[core 11[hwt 0-1]]: [BB/BB/BB/BB/BB/BB/BB/BB/BB/BB/BB/BB][../../../../../../../../../../../..]
[compute-0-17:27300] MCW rank 1 bound to socket 1[core 12[hwt 0-1]], socket 1[core 13[hwt 0-1]], socket 1[core 14[hwt 0-1]], socket 1[core 15[hwt 0-1]], socket 1[core 16[hwt 0-1]], socket 1[core 17[hwt 0-1]], socket 1[core 18[hwt 0-1]], socket 1[core 19[hwt 0-1]], socket 1[core 20[hwt 0-1]], socket 1[core 21[hwt 0-1]], socket 1[core 22[hwt 0-1]], socket 1[core 23[hwt 0-1]]: [../../../../../../../../../../../..][BB/BB/BB/BB/BB/BB/BB/BB/BB/BB/BB/BB]
```
```
 running    2 mpi-ranks, with   12 threads/rank
```
